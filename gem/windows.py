# -*- coding: utf-8 -*-

# ------------------------------------------------------------------
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ------------------------------------------------------------------

# ------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------

# System
from os.path import exists
from os.path import basename
from os.path import splitext
from os.path import expanduser

from glob import glob

# Interface
import gtk

from gtksourceview2 import View
from gtksourceview2 import Buffer
from gtksourceview2 import LanguageManager
from gtksourceview2 import StyleSchemeManager
from gtksourceview2 import print_compositor_new_from_view

from gtk.gdk import Pixbuf
from gtk.gdk import keyval_name
from gtk.gdk import INTERP_TILES
from gtk.gdk import COLORSPACE_RGB
from gtk.gdk import BUTTON_PRESS_MASK
from gtk.gdk import pixbuf_new_from_file

from pango import FontDescription

from utils import *

# Translation
from gettext import gettext
from gettext import textdomain
from gettext import bindtextdomain

# ------------------------------------------------------------------
#   Translation
# ------------------------------------------------------------------

bindtextdomain("gem", get_data("i18n"))
textdomain("gem")

_ = gettext

# ------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------

class TemplateDialog(gtk.Dialog):

    def __init__(self, parent, title, width, height):
        """
        Constructor
        """

        gtk.Dialog.__init__(self, title, parent, gtk.DIALOG_DESTROY_WITH_PARENT)

        # ------------------------------------
        #   Interface
        # ------------------------------------

        self.set_wmclass("gem-dialog", "gem-dialog")
        self.set_icon_name("joystick")

        self.set_resizable(True)

        self.set_default_size(width, height)
        self.set_geometry_hints(None, width, height)
        self.set_position(gtk.WIN_POS_CENTER)

        # ------------------------------------
        #   Grid
        # ------------------------------------

        self.box = self.get_content_area()

        # Properties
        self.box.set_spacing(2)

        self.show_all()


    def start(self):
        """
        Start dialog
        """

        response = self.run()

        self.destroy()

        return response


class TemplateMessageDialog(gtk.MessageDialog):

    def __init__(self, parent, title, subtitle, icon):
        """
        Constructor
        """

        gtk.MessageDialog.__init__(self, parent,
            gtk.DIALOG_DESTROY_WITH_PARENT, icon)

        # ------------------------------------
        #   Interface
        # ------------------------------------

        self.set_title(title)

        self.set_markup("<b>%s</b>" % title)
        self.format_secondary_markup(subtitle)

        self.set_size_request(640, -1)
        self.set_position(gtk.WIN_POS_CENTER)

        # ------------------------------------
        #   Grid
        # ------------------------------------

        self.header_box = self.get_message_area()

        self.box = self.get_content_area()

        # Properties
        self.header_box.set_spacing(2)

        self.box.set_spacing(2)

        self.show_all()

        # ------------------------------------
        #   Set some options for header labels
        # ------------------------------------

        for children in self.header_box.children():
            if type(children) is gtk.Label:
                children.set_selectable(False)
                children.set_justify(gtk.JUSTIFY_FILL)
                children.connect("size-allocate", set_size_request)


    def start(self):
        """
        Start dialog
        """

        self.run()

        self.destroy()


class Dialog(object):

    class Message(TemplateMessageDialog):

        def __init__(self, parent, title, subtitle, icon=None):
            """
            Constructor
            """

            if icon is None:
                icon = gtk.MESSAGE_INFO

            TemplateMessageDialog.__init__(self, parent, title, subtitle, icon)

            self.add_button(gtk.STOCK_CLOSE, gtk.RESPONSE_CLOSE)

            self.set_default_response(gtk.RESPONSE_CLOSE)
            self.get_widget_for_response(gtk.RESPONSE_CLOSE).grab_focus()


    class Error(TemplateMessageDialog):

        def __init__(self, parent, title, subtitle, icon=None):
            """
            Constructor
            """

            if icon is None:
                icon = gtk.MESSAGE_ERROR

            TemplateMessageDialog.__init__(self, parent, title, subtitle, icon)

            self.add_button(gtk.STOCK_CLOSE, gtk.RESPONSE_CLOSE)

            self.set_default_response(gtk.RESPONSE_CLOSE)
            self.get_widget_for_response(gtk.RESPONSE_CLOSE).grab_focus()


    class Warning(TemplateMessageDialog):

        def __init__(self, parent, title, subtitle, icon=None):
            """
            Constructor
            """

            if icon is None:
                icon = gtk.MESSAGE_WARNING

            TemplateMessageDialog.__init__(self, parent, title, subtitle, icon)

            self.add_button(gtk.STOCK_CLOSE, gtk.RESPONSE_CLOSE)

            self.set_default_response(gtk.RESPONSE_CLOSE)
            self.get_widget_for_response(gtk.RESPONSE_CLOSE).grab_focus()


    class Question(TemplateMessageDialog):

        def __init__(self, parent, title, subtitle, icon=None):
            """
            Constructor
            """

            if icon is None:
                icon = gtk.MESSAGE_QUESTION

            TemplateMessageDialog.__init__(self, parent, title, subtitle, icon)

            self.add_button(gtk.STOCK_NO, gtk.RESPONSE_NO)
            self.add_button(gtk.STOCK_YES, gtk.RESPONSE_YES)

            self.set_default_response(gtk.RESPONSE_YES)
            self.get_widget_for_response(gtk.RESPONSE_YES).grab_focus()


    class Input(TemplateMessageDialog):

        def __init__(self, parent, title, subtitle, value=None, default=None):
            """
            Constructor
            """

            TemplateMessageDialog.__init__(
                self, parent, title, subtitle, gtk.MESSAGE_INFO)

            self.add_button(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL)
            self.add_button(gtk.STOCK_OK, gtk.RESPONSE_OK)

            self.set_default_response(gtk.RESPONSE_OK)

            # ------------------------------------
            #   Variables
            # ------------------------------------

            self.value, self.default = value, default

            # ------------------------------------
            #   Widgets
            # ------------------------------------

            self.infobar = gtk.InfoBar()
            self.label_infobar = gtk.Label()

            self.entry = gtk.Entry()

            # Properties
            self.infobar.set_message_type(gtk.MESSAGE_ERROR)
            self.infobar.get_content_area().add(self.label_infobar)

            self.entry.set_activates_default(True)
            self.entry.set_icon_from_stock(1, gtk.STOCK_CLEAR)

            # ------------------------------------
            #   Signals
            # ------------------------------------

            if default is not None:
                self.entry.connect("changed", self.__on_entry_update)

            self.entry.connect("icon-press", on_entry_clear)

            # ------------------------------------
            #   Add widgets into interface
            # ------------------------------------

            self.box.pack_start(self.infobar, True)
            self.box.pack_start(self.entry, True)

            if value is not None:
                self.entry.set_text(value)

            self.infobar.show_all()
            self.infobar.hide()

            self.entry.show_all()
            self.entry.grab_focus()


        def __on_entry_update(self, widget, pos=None, event=None):
            """
            Check if a value is not already used
            """

            text = widget.get_text()

            if text in self.default and not text == self.value:
                self.set_response_sensitive(gtk.RESPONSE_OK, False)

                self.get_image().set_from_stock(
                    gtk.STOCK_DIALOG_ERROR, gtk.ICON_SIZE_DIALOG)

                self.label_infobar.set_text(
                    _("This name is already used, please, choose another."))
                self.infobar.show()

            else:
                self.set_response_sensitive(gtk.RESPONSE_OK, True)

                self.get_image().set_from_stock(
                    gtk.STOCK_DIALOG_INFO, gtk.ICON_SIZE_DIALOG)

                self.infobar.hide()


class Window(object):

    class Editor(TemplateDialog):

        def __init__(self, widget, parent=None):
            """
            Constructor
            """

            self.emulator = parent.consoles.get(
                parent.selection["console"], "emulator")

            # ------------------------------------
            #   Interface
            # ------------------------------------

            TemplateDialog.__init__(self, parent,
                _("Configure %s emulator" % self.emulator), 640, 640)

            # ------------------------------------
            #   Variables
            # ------------------------------------

            self.main_parent = parent

            self.configuration = parent.emulators.get(
                self.emulator, "configuration")

            # ------------------------------------
            #   Initialization
            # ------------------------------------

            # Init widgets
            self.__init_widgets()

            # Start interface
            self.__start_interface()


        def __init_widgets(self):
            """
            Initialize interface widgets
            """

            # ------------------------------------
            #   Interface
            # ------------------------------------

            self.add_button(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL)
            self.add_button(gtk.STOCK_SAVE, gtk.RESPONSE_APPLY)

            # ------------------------------------
            #   Custom configurator
            # ------------------------------------

            if self.main_parent.selection["emulator"] is not None:
                exts = self.main_parent.consoles.item(
                    self.main_parent.selection["console"], "exts")

                if ';' in exts:
                    exts = exts.split(';')[0]

                self.emulator = self.main_parent.selection["emulator"](exts)
                self.emulator.init_values()

                self.box.pack_start(self.emulator, True)

            else:
                # ------------------------------------
                #   Sourceview
                # ------------------------------------

                self.scroll = gtk.ScrolledWindow()

                self.file_buffer = Buffer()

                self.file_language = LanguageManager()
                self.file_style = StyleSchemeManager()

                self.file_view = View(self.file_buffer)
                self.file_compositor = print_compositor_new_from_view(
                    self.file_view)

                # Properties
                self.file_compositor.set_wrap_mode(gtk.WRAP_WORD)
                self.file_compositor.set_tab_width(int(
                    self.main_parent.config.item("editor", "tab", 4)))

                self.file_view.modify_font(FontDescription(
                    self.main_parent.config.item("editor", "font", "Sans 12")))
                self.file_view.set_show_line_numbers(bool(int(
                    self.main_parent.config.item("editor", "lines", 1))))

                self.file_buffer.set_language(
                    self.file_language.guess_language(self.configuration))
                self.file_buffer.set_style_scheme(
                    self.file_style.get_scheme(self.main_parent.config.item(
                    "editor", "colorscheme", "classic")))

                self.scroll.set_policy(
                    gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
                self.scroll.add(self.file_view)
                self.scroll.set_border_width(4)

                # ------------------------------------
                #   Add widgets into interface
                # ------------------------------------

                self.box.pack_start(self.scroll, True)


        def __start_interface(self):
            """
            Load data and start interface
            """

            self.show_all()

            if self.main_parent.selection["emulator"] is not None:
                if self.run() == gtk.RESPONSE_APPLY:
                    self.emulator.get_values()

                self.destroy()

            else:
                if exists(expanduser(self.configuration)):
                    with open(self.configuration, 'r') as pipe:
                        self.file_buffer.set_text(''.join(pipe.readlines()))

                if self.start() == gtk.RESPONSE_APPLY:
                    with open(self.configuration, 'w') as pipe:
                        pipe.write(self.file_buffer.get_text(
                            self.file_buffer.get_start_iter(),
                            self.file_buffer.get_end_iter()))


    class Screenshots(TemplateDialog):

        def __init__(self, parent, title, screenshots_path):
            """
            Constructor
            """

            # ------------------------------------
            #   Interface
            # ------------------------------------

            TemplateDialog.__init__(self, parent, title, 800, 600)

            # ------------------------------------
            #   Variables
            # ------------------------------------

            self.index = 0
            self.zoom_factor = 1

            self.zoom_fit = True

            self.screenshots = screenshots_path

            # ------------------------------------
            #   Initialization
            # ------------------------------------

            # Init widgets
            self.__init_widgets()

            # Init signals
            self.__init_signals()

            # Start interface
            self.__start_interface()


        def __init_widgets(self):
            """
            Initialize interface widgets
            """

            # ------------------------------------
            #   Grid
            # ------------------------------------

            scrollview = gtk.ScrolledWindow()
            view = gtk.Viewport()

            box = gtk.VBox()

            # Properties
            scrollview.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
            view.set_shadow_type(gtk.SHADOW_NONE)

            box.set_homogeneous(False)

            # ------------------------------------
            #   Screenshots viewer
            # ------------------------------------

            self.image = gtk.Image()

            # ------------------------------------
            #   Toolbar
            # ------------------------------------

            self.toolbar = gtk.Toolbar()

            self.button_first = gtk.ToolButton()
            self.button_previous = gtk.ToolButton()
            self.button_zoom_minus = gtk.ToolButton()
            self.button_zoom_100 = gtk.ToolButton()
            self.button_zoom_fit = gtk.ToolButton()
            self.button_zoom_plus = gtk.ToolButton()
            self.button_next = gtk.ToolButton()
            self.button_last = gtk.ToolButton()

            separator_start = gtk.SeparatorToolItem()
            separator_end = gtk.SeparatorToolItem()

            # Properties
            self.toolbar.set_style(gtk.TOOLBAR_ICONS)
            self.toolbar.set_icon_size(gtk.ICON_SIZE_LARGE_TOOLBAR)

            self.button_first.set_stock_id("gtk-goto-first")
            self.button_previous.set_stock_id("gtk-go-back")
            self.button_zoom_minus.set_stock_id("gtk-zoom-out")
            self.button_zoom_100.set_stock_id("gtk-zoom-100")
            self.button_zoom_fit.set_stock_id("gtk-zoom-fit")
            self.button_zoom_plus.set_stock_id("gtk-zoom-in")
            self.button_next.set_stock_id("gtk-go-forward")
            self.button_last.set_stock_id("gtk-goto-last")

            separator_start.set_draw(False)
            separator_start.set_expand(True)
            separator_end.set_draw(False)
            separator_end.set_expand(True)

            # ------------------------------------
            #   Add widgets into interface
            # ------------------------------------

            view.add(box)
            scrollview.add(view)

            self.toolbar.insert(separator_start, -1)
            self.toolbar.insert(self.button_first, -1)
            self.toolbar.insert(self.button_previous, -1)
            self.toolbar.insert(gtk.SeparatorToolItem(), -1)
            self.toolbar.insert(self.button_zoom_minus, -1)
            self.toolbar.insert(self.button_zoom_100, -1)
            self.toolbar.insert(self.button_zoom_fit, -1)
            self.toolbar.insert(self.button_zoom_plus, -1)
            self.toolbar.insert(gtk.SeparatorToolItem(), -1)
            self.toolbar.insert(self.button_next, -1)
            self.toolbar.insert(self.button_last, -1)
            self.toolbar.insert(separator_end, -1)

            box.pack_start(self.image)

            self.box.pack_start(self.toolbar, False)
            self.box.pack_start(scrollview, True)


        def __init_signals(self):
            """
            Initialize widgets signals
            """

            self.button_first.connect("clicked", self.change_screenshot)
            self.button_previous.connect("clicked", self.change_screenshot)
            self.button_next.connect("clicked", self.change_screenshot)
            self.button_last.connect("clicked", self.change_screenshot)

            self.button_zoom_minus.connect("clicked", self.change_screenshot)
            self.button_zoom_100.connect("clicked", self.change_screenshot)
            self.button_zoom_fit.connect("clicked", self.change_screenshot)
            self.button_zoom_plus.connect("clicked", self.change_screenshot)

            self.connect("key-release-event", self.change_screenshot)


        def __start_interface(self):
            """
            Load data and start interface
            """

            self.show_all()

            self.refresh_interface()
            self.update_screenshot()

            self.run()

            self.destroy()


        def change_screenshot(self, widget=None, event=None):
            """
            Change current screenshot
            """

            # Keyboard
            if widget is self:
                if keyval_name(event.keyval) == "Left":
                    self.index -= 1
                elif keyval_name(event.keyval) == "Right":
                    self.index += 1

            # Zoom
            elif widget == self.button_zoom_minus and self.zoom_factor > 1:
                self.zoom_fit = False
                self.zoom_factor -= .1
            elif widget == self.button_zoom_100:
                self.zoom_fit = False
                self.zoom_factor = 1
            elif widget == self.button_zoom_fit:
                self.zoom_fit = True
            elif widget == self.button_zoom_plus and self.zoom_factor < 5:
                self.zoom_fit = False
                self.zoom_factor += .1

            # Move
            elif widget == self.button_first:
                self.index = 0
            elif widget == self.button_previous:
                self.index -= 1
            elif widget == self.button_next:
                self.index += 1
            elif widget == self.button_last:
                self.index = len(self.screenshots) - 1

            if self.index < 0:
                self.index = 0
            elif self.index > len(self.screenshots) - 1:
                self.index = len(self.screenshots) - 1

            if self.zoom_factor < 1:
                self.zoom_factor = 1
            elif self.zoom_factor > 5:
                self.zoom_factor = 5

            self.update_screenshot()
            self.refresh_interface()


        def refresh_interface(self):
            """
            Refresh interface's widgets
            """

            self.button_first.set_sensitive(True)
            self.button_previous.set_sensitive(True)
            self.button_next.set_sensitive(True)
            self.button_last.set_sensitive(True)

            if self.index == 0:
                self.button_first.set_sensitive(False)
                self.button_previous.set_sensitive(False)

            if self.index == len(self.screenshots) - 1:
                self.button_next.set_sensitive(False)
                self.button_last.set_sensitive(False)


        def update_screenshot(self):
            """
            Change current screenshot size
            """

            pixbuf = pixbuf_new_from_file(
                expanduser(self.screenshots[self.index]))

            width, height = pixbuf.get_width(), pixbuf.get_height()

            if self.zoom_fit:
                allocation = self.get_allocation()

                ratio_x = float(allocation.width / width)
                ratio_y = float(allocation.height / height)

                self.zoom_factor = min(ratio_x, ratio_y)

            self.image.set_from_pixbuf(pixbuf.scale_simple(
                int(self.zoom_factor * width), int(self.zoom_factor * height),
                INTERP_TILES))


    class ROM(TemplateMessageDialog):

        def __init__(self, parent, title, subtitle, data, emulators):
            """
            Constructor
            """

            TemplateMessageDialog.__init__(self, parent, title, subtitle,
                gtk.MESSAGE_INFO)

            # ------------------------------------
            #   Variables
            # ------------------------------------

            self.arguments, self.emulator = str(), str()
            if data is not None:
                self.arguments, self.emulator = data

            self.emulators = emulators

            # ------------------------------------
            #   Initialization
            # ------------------------------------

            # Init widgets
            self.__init_widgets()


        def __init_widgets(self):
            """
            Initialize interface widgets
            """

            # ------------------------------------
            #   Interface
            # ------------------------------------

            self.add_button(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL)
            self.add_button(gtk.STOCK_OK, gtk.RESPONSE_OK)

            self.set_default_response(gtk.RESPONSE_OK)

            # ------------------------------------
            #   Grid
            # ------------------------------------

            grid = gtk.Table()

            # Properties
            grid.set_border_width(8)
            grid.set_row_spacings(8)
            grid.set_col_spacings(8)
            grid.set_homogeneous(False)

            # ------------------------------------
            #   Widgets
            # ------------------------------------

            label_emulator = gtk.Label()
            self.combo_emulator = gtk.ComboBox()

            self.model_emulator = gtk.ListStore(str)
            cell_emulator = gtk.CellRendererText()

            label_arguments = gtk.Label()
            self.entry_arguments = gtk.Entry()

            # Properties
            label_emulator.set_alignment(0, .5)
            label_emulator.set_label(_("Emulator"))

            self.combo_emulator.set_model(self.model_emulator)
            self.combo_emulator.pack_start(cell_emulator, True)
            self.combo_emulator.add_attribute(cell_emulator, "text", 0)

            label_arguments.set_alignment(0, .5)
            label_arguments.set_label(_("Arguments"))

            self.entry_arguments.set_icon_from_stock(1, gtk.STOCK_CLEAR)

            # ------------------------------------
            #   Signals
            # ------------------------------------

            self.entry_arguments.connect("icon-press", on_entry_clear)

            # ------------------------------------
            #   Add widgets into interface
            # ------------------------------------

            grid.attach(label_emulator, 0, 1, 0, 1, gtk.FILL)
            grid.attach(self.combo_emulator, 1, 2, 0, 1)
            grid.attach(label_arguments, 0, 1, 1, 2, gtk.FILL)
            grid.attach(self.entry_arguments, 1, 2, 1, 2)

            self.box.pack_start(grid, True)

            self.show_all()

            self.model_emulator.append([str()])

            for emulator in self.emulators:
                row = self.model_emulator.append([emulator])

                if emulator == self.emulator:
                    self.combo_emulator.set_active_iter(row)

            self.entry_arguments.set_text(self.arguments)


    class Remove(TemplateMessageDialog):

        def __init__(self, parent, title, subtitle):
            """
            Constructor
            """

            TemplateMessageDialog.__init__(self, parent, title, subtitle,
                gtk.MESSAGE_WARNING)

            # ------------------------------------
            #   Initialization
            # ------------------------------------

            # Init widgets
            self.__init_widgets()


        def __init_widgets(self):
            """
            Initialize interface widgets
            """

            # ------------------------------------
            #   Interface
            # ------------------------------------

            self.add_button(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL)
            self.add_button(gtk.STOCK_OK, gtk.RESPONSE_OK)

            self.set_default_response(gtk.RESPONSE_OK)

            # ------------------------------------
            #   Grids
            # ------------------------------------

            self.box.set_spacing(8)
            self.box.set_border_width(8)

            # ------------------------------------
            #   Widgets
            # ------------------------------------

            self.check_database = gtk.CheckButton()
            self.check_save = gtk.CheckButton()
            self.check_screenshots = gtk.CheckButton()

            # Properties
            self.check_database.set_label(_("Remove game data from database"))
            self.check_save.set_label(_("Remove save files"))
            self.check_screenshots.set_label(_("Remove screenshots"))

            # ------------------------------------
            #   Signals
            # ------------------------------------

            # ------------------------------------
            #   Add widgets into interface
            # ------------------------------------

            self.box.pack_start(self.check_database, False)
            self.box.pack_start(gtk.HSeparator(), True)
            self.box.pack_start(self.check_save, False)
            self.box.pack_start(self.check_screenshots, False)

            self.show_all()

            self.check_database.set_active(True)


    class Icon(TemplateDialog):

        def __init__(self, parent, title, path=None):
            """
            Constructor
            """

            TemplateDialog.__init__(self, parent, title, 800, 600)

            # ------------------------------------
            #   Variables
            # ------------------------------------

            self.path = path
            self.new_path = None

            if parent is None:
                self.empty = Pixbuf(COLORSPACE_RGB, True, 8, 24, 24)
                self.empty.fill(0x00000000)
            else:
                self.empty = parent.empty

            # ------------------------------------
            #   Initialization
            # ------------------------------------

            # Init widgets
            self.__init_widgets()

            # Init signals
            self.__init_signals()

            # Start interface
            self.__start_interface()


        def __init_widgets(self):
            """
            Initialize interface widgets
            """

            # ------------------------------------
            #   Interface
            # ------------------------------------

            self.add_buttons(gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT)
            self.add_buttons(gtk.STOCK_OK, gtk.RESPONSE_ACCEPT)

            # ------------------------------------
            #   Grid
            # ------------------------------------

            scrollview = gtk.ScrolledWindow()
            view = gtk.Viewport()

            box = gtk.Table()

            # Properties
            scrollview.set_border_width(4)
            scrollview.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
            view.set_shadow_type(gtk.SHADOW_NONE)

            box.set_border_width(4)
            box.set_row_spacings(8)
            box.set_col_spacings(8)
            box.set_homogeneous(False)

            # ------------------------------------
            #   Option
            # ------------------------------------

            label_option = gtk.Label()

            self.model_option = gtk.ListStore(str)
            self.combo_option = gtk.ComboBox()

            cell_option = gtk.CellRendererText()

            # Properties
            label_option.set_alignment(1, .5)
            label_option.set_label(_("Select icon from"))

            self.combo_option.set_model(self.model_option)
            self.combo_option.pack_start(cell_option, True)
            self.combo_option.add_attribute(cell_option, "text", 0)

            # ------------------------------------
            #   Custom
            # ------------------------------------

            self.file_icon = gtk.FileChooserWidget()

            # Properties
            self.file_icon.set_current_folder(expanduser('~'))

            # ------------------------------------
            #   Icons
            # ------------------------------------

            self.view_icons = gtk.IconView()
            self.model_icons = gtk.ListStore(Pixbuf, str)

            self.scroll_icons = gtk.ScrolledWindow()

            # Properties
            self.view_icons.set_model(self.model_icons)
            self.view_icons.set_pixbuf_column(0)
            self.view_icons.set_text_column(1)
            self.view_icons.set_item_width(96)
            self.view_icons.set_selection_mode(gtk.SELECTION_SINGLE)

            self.model_icons.set_sort_column_id(1, gtk.SORT_ASCENDING)

            self.scroll_icons.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)

            # ------------------------------------
            #   Add widgets into interface
            # ------------------------------------

            view.add(box)
            scrollview.add(view)

            self.scroll_icons.add(self.view_icons)

            box.attach(label_option, 0, 1, 0, 1, gtk.FILL, gtk.FILL)
            box.attach(self.combo_option, 1, 2, 0, 1, yoptions=gtk.FILL)
            box.attach(self.file_icon, 0, 2, 1, 2)
            box.attach(self.scroll_icons, 0, 2, 2, 3)

            self.box.pack_start(scrollview, True)


        def __init_signals(self):
            """
            Initialize widgets signals
            """

            self.combo_option.connect("changed", self.set_sensitive)


        def __start_interface(self):
            """
            Load data and start interface
            """

            self.show_all()

            self.load_interface()

            response = self.run()

            if response == gtk.RESPONSE_ACCEPT:
                self.save_interface()

            self.destroy()


        def load_interface(self):
            """
            Insert data into interface's widgets
            """

            # Fill options combobox
            self.model_option.append([_("All icons")])
            self.model_option.append([_("Image file")])

            self.combo_option.set_active(0)

            self.file_icon.set_visible(False)
            self.scroll_icons.set_visible(True)

            self.icons_data = dict()

            # Fill icons view
            for icon in glob("%s/*.%s" % (get_data("icons"), Icons.Ext)):
                name = splitext(basename(icon))[0]

                self.icons_data[name] = self.model_icons.append([
                    icon_from_data(icon, self.empty, 48, 48), name])

            # Set filechooser or icons view selected item
            if self.path is not None:

                # Check if current path is a gem icons
                data = "%s/%s.%s" % (get_data("icons"), self.path, Icons.Ext)
                if data is not None and exists(data):
                    self.view_icons.select_path(
                        self.model_icons.get_path(self.icons_data[self.path]))

                else:
                    self.file_icon.set_filename(self.path)


        def save_interface(self):
            """
            Return all the data from interface
            """

            if self.combo_option.get_active_text() == _("All icons"):
                selection = self.view_icons.get_selected_items()[0]

                path = self.model_icons.get_value(
                    self.model_icons.get_iter(selection), 1)

            else:
                path = self.file_icon.get_filename()

            if not path == self.path:
                self.new_path = path


        def set_sensitive(self, widget):
            """
            Change sensitive state between radio children
            """

            if widget.get_active_text() == _("All icons"):
                self.file_icon.set_visible(False)
                self.scroll_icons.set_visible(True)

            else:
                self.file_icon.set_visible(True)
                self.scroll_icons.set_visible(False)


    class Default(TemplateDialog):

        def __init__(self, parent, title, width=800, height=600):
            """
            Constructor
            """

            TemplateDialog.__init__(self, parent, title, width, height)

            # ------------------------------------
            #   Variables
            # ------------------------------------

            if parent is None:
                self.empty = Pixbuf(COLORSPACE_RGB, True, 8, 24, 24)
                self.empty.fill(0x00000000)
            else:
                self.empty = parent.empty

            # ------------------------------------
            #   Initialization
            # ------------------------------------

            # Init widgets
            self.__init_widgets()


        def __init_widgets(self):
            """
            Initialize interface widgets
            """

            # ------------------------------------
            #   Interface
            # ------------------------------------

            self.add_buttons(gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT)
            self.add_buttons(gtk.STOCK_SAVE, gtk.RESPONSE_ACCEPT)

            # ------------------------------------
            #   Infobar
            # ------------------------------------

            self.infobar = gtk.InfoBar()
            self.label_infobar = gtk.Label()

            # Properties
            self.infobar.set_message_type(gtk.MESSAGE_ERROR)
            self.infobar.get_content_area().add(self.label_infobar)

            # ------------------------------------
            #   Add widgets into interface
            # ------------------------------------

            self.box.pack_start(self.infobar, False)


        def select_icon(self, widget):
            """
            Select a new icon
            """

            dialog = Window.Icon(self, _("Choose an icon"), self.path)

            if dialog.new_path is not None:
                widget.get_image().set_from_pixbuf(
                    icon_from_data(dialog.new_path, self.empty, 64, 64))

                self.path = dialog.new_path


        def start(self):
            """
            Load data and start interface
            """

            self.section, self.data = None, None

            response = self.run()

            if response == gtk.RESPONSE_ACCEPT:
                self.save_interface()

            self.destroy()

            return self.section, self.data


        def set_infobar(self, text=None):
            """
            Set infobar message
            """

            if text is not None:
                self.label_infobar.set_text(text)
                self.infobar.show()

            else:
                self.infobar.hide()
