# -*- coding: utf-8 -*-

# ------------------------------------------------------------------
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ------------------------------------------------------------------

# ------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------

# System
from os.path import exists
from os.path import basename
from os.path import splitext
from os.path import expanduser
from os.path import join as path_join

# Translation
from gettext import lgettext as _
from gettext import textdomain
from gettext import bindtextdomain

# Interface
import gtk
import gtksourceview2

from pango import Style
from pango import STYLE_ITALIC
from pango import STYLE_NORMAL
from pango import WRAP_WORD_CHAR

from gtk.gdk import Pixbuf
from gtk.gdk import COLORSPACE_RGB

from glib import GError

from utils import *
from windows import *
from configuration import Configuration

# ------------------------------------------------------------------
#   Translation
# ------------------------------------------------------------------

bindtextdomain("gem", get_data("i18n"))
textdomain("gem")

# ------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------

class Preferences(Window.Default):

    def __init__(self, widget=None, parent=None):
        """
        Constructor
        """

        Window.Default.__init__(self, parent, _("Preferences"))

        # ------------------------------------
        #   Variables
        # ------------------------------------

        self.interface = parent

        self.need_reload = False

        self.shortcuts = {
            "start": [_("Launch a game"), "Return"],
            "remove": [_("Remove a game from database"), "Delete"],
            "delete": [_("Remove a game from disk"), "<Control>Delete"],
            "rename": [_("Rename a game"), "F2"],
            "favorite": [_("Mark a game as favorite"), "F3"],
            "multiplayer": [_("Mark a game as multiplayer"), "F4"],
            "snapshots": [_("Show game snapshots"), "F5"],
            "exceptions": [_("Set specific arguments for a game"), "F12"],
            "open": [_("Open selected game directory"), "<Control>O"],
            "copy": [_("Copy selected game path"), "<Control>C"],
            "preferences": [_("Open preferences"), "<Control>P"],
            "quit": [_("Quit application"), "<Control>Q"] }

        self.lines = {
            _("None"): "none",
            _("Horizontal"): "horizontal",
            _("Vertical"): "vertical",
            _("Both"): "both" }

        # ------------------------------------
        #   Configuration
        # ------------------------------------

        if self.interface is not None:
            self.config = self.interface.config
            self.consoles = self.interface.consoles
            self.emulators = self.interface.emulators

        else:
            self.config = Configuration(
                expanduser(path_join(Conf.User, "gem.conf")))
            self.consoles = Configuration(
                expanduser(path_join(Conf.User, "consoles.conf")))
            self.emulators = Configuration(
                expanduser(path_join(Conf.User, "emulators.conf")))

        # ------------------------------------
        #   Initialization
        # ------------------------------------

        # Init widgets
        self.__init_widgets()

        # Init signals
        self.__init_signals()

        # Start interface
        self.__start_interface()


    def __init_widgets(self):
        """
        Initialize interface widgets
        """

        # ------------------------------------
        #   Grids
        # ------------------------------------

        box = self.get_content_area()

        self.box_tab_interface = gtk.HBox()
        self.box_tab_misc = gtk.HBox()
        self.box_tab_shortcuts = gtk.HBox()
        self.box_tab_emulators = gtk.HBox()
        self.box_tab_consoles = gtk.HBox()

        scrollview_interface = gtk.ScrolledWindow()
        view_interface = gtk.Viewport()

        box_interface = gtk.VBox()
        box_interface_general = gtk.VBox()
        box_interface_general_list = gtk.HBox()
        box_interface_general_columns = gtk.VBox()
        box_misc_viewer = gtk.Table()
        box_misc_editor = gtk.Table()

        scrollview_misc = gtk.ScrolledWindow()
        view_misc = gtk.Viewport()
        box_misc = gtk.VBox()

        scrollview_shortcuts = gtk.ScrolledWindow()
        view_shortcuts = gtk.Viewport()
        box_shortcuts = gtk.VBox()

        scrollview_emulators = gtk.ScrolledWindow()
        view_emulators = gtk.Viewport()
        box_emulators = gtk.VBox()

        scrollview_consoles = gtk.ScrolledWindow()
        view_consoles = gtk.Viewport()
        box_consoles = gtk.VBox()

        # Properties
        box.set_spacing(2)

        self.box_tab_interface.set_spacing(8)
        self.box_tab_misc.set_spacing(8)
        self.box_tab_shortcuts.set_spacing(8)
        self.box_tab_emulators.set_spacing(8)
        self.box_tab_consoles.set_spacing(8)

        scrollview_interface.set_border_width(4)
        scrollview_interface.set_policy(
            gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        view_interface.set_shadow_type(gtk.SHADOW_NONE)

        box_interface_general.set_spacing(8)
        box_interface_general.set_border_width(8)
        box_interface_general.set_homogeneous(False)

        box_interface_general_list.set_spacing(8)
        box_interface_general_list.set_border_width(8)
        box_interface_general_list.set_homogeneous(False)

        box_interface_general_columns.set_spacing(8)
        box_interface_general_columns.set_border_width(8)
        box_interface_general_columns.set_homogeneous(False)

        scrollview_misc.set_border_width(4)
        scrollview_misc.set_policy(
            gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)
        view_misc.set_shadow_type(gtk.SHADOW_NONE)

        # box_misc.set_border_width(8)
        box_misc.set_spacing(8)

        box_misc_viewer.set_border_width(8)
        box_misc_viewer.set_row_spacings(8)
        box_misc_viewer.set_col_spacings(8)
        box_misc_viewer.set_homogeneous(False)
        box_misc_editor.set_border_width(8)
        box_misc_editor.set_row_spacings(8)
        box_misc_editor.set_col_spacings(8)
        box_misc_editor.set_homogeneous(False)

        scrollview_shortcuts.set_border_width(4)
        scrollview_shortcuts.set_policy(
            gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)
        view_shortcuts.set_shadow_type(gtk.SHADOW_NONE)

        box_shortcuts.set_border_width(4)
        box_shortcuts.set_spacing(8)

        scrollview_emulators.set_border_width(4)
        scrollview_emulators.set_policy(
            gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        view_emulators.set_shadow_type(gtk.SHADOW_NONE)

        box_emulators.set_border_width(4)
        box_emulators.set_spacing(2)

        scrollview_consoles.set_border_width(4)
        scrollview_consoles.set_policy(
            gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        view_consoles.set_shadow_type(gtk.SHADOW_NONE)

        box_consoles.set_border_width(4)
        box_consoles.set_spacing(2)

        # ------------------------------------
        #   Notebook
        # ------------------------------------

        notebook = gtk.Notebook()

        label_interface = gtk.Label()
        image_interface = gtk.Image()

        label_misc = gtk.Label()
        image_misc = gtk.Image()

        label_shortcuts = gtk.Label()
        image_shortcuts = gtk.Image()

        label_emulators = gtk.Label()
        image_emulators = gtk.Image()

        label_consoles = gtk.Label()
        image_consoles = gtk.Image()

        # Properties
        notebook.set_border_width(4)
        notebook.set_tab_pos(gtk.POS_LEFT)

        label_interface.set_label(_("Interface"))
        image_interface.set_from_icon_name(Icons.System, gtk.ICON_SIZE_MENU)

        label_misc.set_label(_("Misc"))
        image_misc.set_from_icon_name(Icons.Misc, gtk.ICON_SIZE_MENU)

        label_shortcuts.set_label(_("Shortcuts"))
        image_shortcuts.set_from_icon_name(Icons.Keyboard, gtk.ICON_SIZE_MENU)

        label_consoles.set_label(_("Consoles"))
        image_consoles.set_from_icon_name(Icons.Game, gtk.ICON_SIZE_MENU)

        label_emulators.set_label(_("Emulators"))
        image_emulators.set_from_icon_name(Icons.Emulator, gtk.ICON_SIZE_MENU)

        # ------------------------------------
        #   General
        # ------------------------------------

        frame_general = gtk.Frame()
        label_frame_general = gtk.Label()

        self.check_load_console = gtk.CheckButton()

        self.check_output = gtk.CheckButton()
        self.check_statusbar = gtk.CheckButton()

        # Properties
        frame_general.set_label_widget(label_frame_general)
        frame_general.set_border_width(4)

        label_frame_general.set_padding(4, -1)
        label_frame_general.set_use_markup(True)
        label_frame_general.set_markup("<b>%s</b>" % _("General"))

        self.check_load_console.set_label(
            _("Load the last chosen console during startup"))

        self.check_output.set_label(_("Show output tab"))
        self.check_statusbar.set_label(_("Show statusbar"))

        # ------------------------------------
        #   List
        # ------------------------------------

        frame_general_list = gtk.Frame()
        label_frame_general_list = gtk.Label()

        label_lines = gtk.Label()
        self.model_lines = gtk.ListStore(str)
        self.combo_lines = gtk.ComboBox()

        cell_lines = gtk.CellRendererText()

        # Properties
        frame_general_list.set_label_widget(label_frame_general_list)
        frame_general_list.set_border_width(4)

        label_frame_general_list.set_padding(4, -1)
        label_frame_general_list.set_use_markup(True)
        label_frame_general_list.set_markup("<b>%s</b>" % _("Games list"))

        label_lines.set_alignment(0, .5)
        label_lines.set_text(_("Show lines in games list"))

        self.combo_lines.set_model(self.model_lines)

        self.combo_lines.pack_start(cell_lines, True)
        self.combo_lines.add_attribute(cell_lines, "text", 0)

        self.model_lines.set_sort_column_id(0, gtk.SORT_ASCENDING)

        # ------------------------------------
        #   Columns
        # ------------------------------------

        frame_general_columns = gtk.Frame()
        label_frame_general_columns = gtk.Label()

        self.check_column_play = gtk.CheckButton()
        self.check_column_last_play = gtk.CheckButton()
        self.check_column_play_time = gtk.CheckButton()
        self.check_column_installed = gtk.CheckButton()
        self.check_column_flags = gtk.CheckButton()

        # Properties
        frame_general_columns.set_label_widget(label_frame_general_columns)
        frame_general_columns.set_border_width(4)

        label_frame_general_columns.set_padding(4, -1)
        label_frame_general_columns.set_use_markup(True)
        label_frame_general_columns.set_markup("<b>%s</b>" % _("Columns"))

        self.check_column_play.set_label(_("Show \"Play\" column"))
        self.check_column_last_play.set_label(_("Show \"Last play\" column"))
        self.check_column_play_time.set_label(_("Show \"Play time\" column"))
        self.check_column_installed.set_label(_("Show \"Installed\" column"))
        self.check_column_flags.set_label(_("Show \"Flags\" column"))

        # ------------------------------------
        #   Viewer
        # ------------------------------------

        frame_viewer = gtk.Frame()
        label_frame_viewer = gtk.Label()

        self.check_viewer = gtk.CheckButton()

        label_viewer = gtk.Label()
        self.file_viewer = gtk.FileChooserButton(_("Choose your viewer"))

        label_arguments = gtk.Label()
        self.entry_arguments = gtk.Entry()

        label_font = gtk.Label()
        self.file_font = gtk.FontButton()

        # Properties
        frame_viewer.set_label_widget(label_frame_viewer)
        frame_viewer.set_border_width(4)

        label_frame_viewer.set_padding(4, -1)
        label_frame_viewer.set_use_markup(True)
        label_frame_viewer.set_markup("<b>%s</b>" % _("Screenshots viewer"))

        self.check_viewer.set_label(_("Use native viewer"))

        label_viewer.set_alignment(0, .5)
        label_viewer.set_text(_("Binary"))

        label_arguments.set_alignment(0, .5)
        label_arguments.set_text(_("Default options"))

        self.entry_arguments.set_size_request(300, -1)
        self.entry_arguments.set_icon_from_stock(1, gtk.STOCK_CLEAR)

        label_font.set_alignment(0, .5)
        label_font.set_text(_("Default font"))

        # ------------------------------------
        #   Editor
        # ------------------------------------

        frame_editor = gtk.Frame()
        label_frame_editor = gtk.Label()

        self.check_editor_lines = gtk.CheckButton()

        label_editor_colorscheme = gtk.Label()

        self.model_editor_colorscheme = gtk.ListStore(str)
        self.combo_editor_colorscheme = gtk.ComboBox()

        cell_editor_colorscheme = gtk.CellRendererText()

        # Properties
        frame_editor.set_label_widget(label_frame_editor)
        frame_editor.set_border_width(4)

        label_frame_editor.set_padding(4, -1)
        label_frame_editor.set_use_markup(True)
        label_frame_editor.set_markup("<b>%s</b>" % _("Editor"))

        self.check_editor_lines.set_label(_("Show line numbers"))

        label_editor_colorscheme.set_alignment(0, .5)
        label_editor_colorscheme.set_text(_("Colorscheme"))

        self.combo_editor_colorscheme.set_model(self.model_editor_colorscheme)

        self.combo_editor_colorscheme.pack_start(cell_editor_colorscheme, True)
        self.combo_editor_colorscheme.add_attribute(
            cell_editor_colorscheme, "text", 0)

        self.model_editor_colorscheme.set_sort_column_id(0, gtk.SORT_ASCENDING)

        # ------------------------------------
        #   Shortcuts
        # ------------------------------------

        self.label_shortcuts_explanation = gtk.Label()

        scroll_shortcuts = gtk.ScrolledWindow()

        self.model_shortcuts = gtk.ListStore(str, str, str)
        self.treeview_shortcuts = gtk.TreeView()

        column_shortcuts_name = gtk.TreeViewColumn()
        column_shortcuts_keys = gtk.TreeViewColumn()

        cell_shortcuts_name = gtk.CellRendererText()
        self.cell_shortcuts_keys = gtk.CellRendererAccel()

        # Properties
        self.label_shortcuts_explanation.set_line_wrap(True)
        self.label_shortcuts_explanation.set_single_line_mode(False)
        self.label_shortcuts_explanation.set_justify(gtk.JUSTIFY_FILL)
        self.label_shortcuts_explanation.set_text(_("You can edit interface "
            "shortcuts for some actions. Click on a shortcut and insert "
            "wanted shortcut with your keyboard."))

        scroll_shortcuts.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

        self.model_shortcuts.set_sort_column_id(0, gtk.SORT_ASCENDING)

        self.treeview_shortcuts.set_model(self.model_shortcuts)

        column_shortcuts_name.set_expand(True)
        column_shortcuts_name.set_title(_("Action"))
        column_shortcuts_name.set_resizable(False)
        column_shortcuts_name.set_sizing(gtk.TREE_VIEW_COLUMN_AUTOSIZE)
        column_shortcuts_name.pack_start(cell_shortcuts_name, True)
        column_shortcuts_name.set_attributes(cell_shortcuts_name, text=0)

        self.cell_shortcuts_keys.set_property("editable", True)

        column_shortcuts_keys.set_expand(True)
        column_shortcuts_keys.set_title(_("Shortcut"))
        column_shortcuts_keys.set_resizable(False)
        column_shortcuts_keys.set_sizing(gtk.TREE_VIEW_COLUMN_AUTOSIZE)
        column_shortcuts_keys.pack_start(self.cell_shortcuts_keys, True)
        column_shortcuts_keys.set_attributes(self.cell_shortcuts_keys, text=1)

        self.treeview_shortcuts.append_column(column_shortcuts_name)
        self.treeview_shortcuts.append_column(column_shortcuts_keys)

        scroll_shortcuts.add(self.treeview_shortcuts)

        # ------------------------------------
        #   Consoles
        # ------------------------------------

        buttons_consoles = gtk.HButtonBox()
        self.button_consoles_append = gtk.Button()
        self.button_consoles_rename = gtk.Button()
        self.button_consoles_remove = gtk.Button()

        scroll_consoles = gtk.ScrolledWindow()

        self.model_consoles = gtk.ListStore(Pixbuf, str, str)
        self.treeview_consoles = gtk.TreeView()

        column_consoles_icon = gtk.TreeViewColumn()
        column_consoles_name = gtk.TreeViewColumn()
        column_consoles_emulator = gtk.TreeViewColumn()

        cell_consoles_icon = gtk.CellRendererPixbuf()
        cell_consoles_name = gtk.CellRendererText()
        cell_consoles_emulator = gtk.CellRendererText()

        # Properties
        buttons_consoles.set_layout(gtk.BUTTONBOX_END)

        self.button_consoles_append.set_use_stock(True)
        self.button_consoles_append.set_label(gtk.STOCK_ADD)
        self.button_consoles_rename.set_label(_("Rename"))
        self.button_consoles_rename.set_image(gtk.image_new_from_stock(
            gtk.STOCK_EDIT, gtk.ICON_SIZE_BUTTON))
        self.button_consoles_remove.set_use_stock(True)
        self.button_consoles_remove.set_label(gtk.STOCK_REMOVE)

        scroll_consoles.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

        self.model_consoles.set_sort_column_id(1, gtk.SORT_ASCENDING)

        self.treeview_consoles.set_model(self.model_consoles)

        column_consoles_icon.set_resizable(False)
        column_consoles_icon.pack_start(cell_consoles_icon, False)
        column_consoles_icon.add_attribute(cell_consoles_icon, "pixbuf", 0)

        column_consoles_name.set_expand(True)
        column_consoles_name.set_title(_("Name"))
        column_consoles_name.set_resizable(False)
        column_consoles_name.set_sizing(gtk.TREE_VIEW_COLUMN_AUTOSIZE)
        column_consoles_name.pack_start(cell_consoles_name, True)
        column_consoles_name.set_attributes(cell_consoles_name, text=1)

        column_consoles_emulator.set_expand(True)
        column_consoles_emulator.set_title(_("Emulator"))
        column_consoles_emulator.set_resizable(False)
        column_consoles_emulator.set_sizing(gtk.TREE_VIEW_COLUMN_AUTOSIZE)
        column_consoles_emulator.pack_start(cell_consoles_emulator, True)
        column_consoles_emulator.set_attributes(cell_consoles_emulator, text=2)

        self.treeview_consoles.append_column(column_consoles_icon)
        self.treeview_consoles.append_column(column_consoles_name)
        self.treeview_consoles.append_column(column_consoles_emulator)

        scroll_consoles.add(self.treeview_consoles)

        # ------------------------------------
        #   Emulators
        # ------------------------------------

        buttons_emulators = gtk.HButtonBox()
        self.button_emulators_append = gtk.Button()
        self.button_emulators_rename = gtk.Button()
        self.button_emulators_remove = gtk.Button()

        scroll_emulators = gtk.ScrolledWindow()

        self.model_emulators = gtk.ListStore(Pixbuf, str, str)
        self.treeview_emulators = gtk.TreeView()

        column_emulators_icon = gtk.TreeViewColumn()
        column_emulators_name = gtk.TreeViewColumn()
        column_emulators_binary = gtk.TreeViewColumn()

        cell_emulators_icon = gtk.CellRendererPixbuf()
        cell_emulators_name = gtk.CellRendererText()
        cell_emulators_binary = gtk.CellRendererText()

        # Properties
        buttons_emulators.set_layout(gtk.BUTTONBOX_END)

        self.button_emulators_append.set_use_stock(True)
        self.button_emulators_append.set_label(gtk.STOCK_ADD)
        self.button_emulators_rename.set_label(_("Rename"))
        self.button_emulators_rename.set_image(gtk.image_new_from_stock(
            gtk.STOCK_EDIT, gtk.ICON_SIZE_BUTTON))
        self.button_emulators_remove.set_use_stock(True)
        self.button_emulators_remove.set_label(gtk.STOCK_REMOVE)

        scroll_emulators.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

        self.model_emulators.set_sort_column_id(1, gtk.SORT_ASCENDING)

        self.treeview_emulators.set_model(self.model_emulators)

        column_emulators_icon.set_resizable(False)
        column_emulators_icon.pack_start(cell_emulators_icon, False)
        column_emulators_icon.add_attribute(cell_emulators_icon, "pixbuf", 0)

        column_emulators_name.set_expand(True)
        column_emulators_name.set_title(_("Name"))
        column_emulators_name.set_resizable(False)
        column_emulators_name.set_sizing(gtk.TREE_VIEW_COLUMN_AUTOSIZE)
        column_emulators_name.pack_start(cell_emulators_name, True)
        column_emulators_name.set_attributes(cell_emulators_name, text=1)

        column_emulators_binary.set_expand(True)
        column_emulators_binary.set_title(_("Binary"))
        column_emulators_binary.set_resizable(False)
        column_emulators_binary.set_sizing(gtk.TREE_VIEW_COLUMN_AUTOSIZE)
        column_emulators_binary.pack_start(cell_emulators_binary, True)
        column_emulators_binary.set_attributes(cell_emulators_binary, text=2)

        self.treeview_emulators.append_column(column_emulators_icon)
        self.treeview_emulators.append_column(column_emulators_name)
        self.treeview_emulators.append_column(column_emulators_binary)

        scroll_emulators.add(self.treeview_emulators)

        # ------------------------------------
        #   Add widgets into interface
        # ------------------------------------

        box.pack_start(notebook, True)

        view_interface.add(box_interface)
        scrollview_interface.add(view_interface)

        view_misc.add(box_misc)
        scrollview_misc.add(view_misc)

        view_shortcuts.add(box_shortcuts)
        scrollview_shortcuts.add(view_shortcuts)

        view_consoles.add(box_consoles)
        scrollview_consoles.add(view_consoles)

        view_emulators.add(box_emulators)
        scrollview_emulators.add(view_emulators)

        notebook.append_page(scrollview_interface, self.box_tab_interface)
        notebook.append_page(scrollview_misc, self.box_tab_misc)
        notebook.append_page(scrollview_shortcuts, self.box_tab_shortcuts)
        notebook.append_page(scrollview_consoles, self.box_tab_consoles)
        notebook.append_page(scrollview_emulators, self.box_tab_emulators)

        # Notebook
        self.box_tab_interface.pack_start(image_interface, False)
        self.box_tab_interface.pack_start(label_interface, False)

        self.box_tab_misc.pack_start(image_misc, False)
        self.box_tab_misc.pack_start(label_misc, False)

        self.box_tab_shortcuts.pack_start(image_shortcuts, False)
        self.box_tab_shortcuts.pack_start(label_shortcuts, False)

        self.box_tab_consoles.pack_start(image_consoles, False)
        self.box_tab_consoles.pack_start(label_consoles, False)

        self.box_tab_emulators.pack_start(image_emulators, False)
        self.box_tab_emulators.pack_start(label_emulators, False)

        # Interface
        box_interface_general.pack_start(self.check_load_console, False)
        box_interface_general.pack_start(gtk.HSeparator(), False)
        box_interface_general.pack_start(self.check_output, False)
        box_interface_general.pack_start(self.check_statusbar, False)

        frame_general.add(box_interface_general)

        box_interface_general_list.pack_start(label_lines, True)
        box_interface_general_list.pack_start(self.combo_lines, True)

        frame_general_list.add(box_interface_general_list)

        box_interface_general_columns.pack_start(
            self.check_column_play, False)
        box_interface_general_columns.pack_start(
            self.check_column_last_play, False)
        box_interface_general_columns.pack_start(
            self.check_column_play_time, False)
        box_interface_general_columns.pack_start(
            self.check_column_installed, False)
        box_interface_general_columns.pack_start(
            self.check_column_flags, False)

        frame_general_columns.add(box_interface_general_columns)

        box_interface.pack_start(frame_general, False)
        box_interface.pack_start(frame_general_list, False)
        box_interface.pack_start(frame_general_columns, False)

        # Misc
        box_misc_viewer.attach(self.check_viewer, 0, 2, 0, 1)
        box_misc_viewer.attach(gtk.HSeparator(), 0, 2, 1, 2)
        box_misc_viewer.attach(label_viewer, 0, 1, 2, 3)
        box_misc_viewer.attach(self.file_viewer, 1, 2, 2, 3)
        box_misc_viewer.attach(label_arguments, 0, 1, 3, 4)
        box_misc_viewer.attach(self.entry_arguments, 1, 2, 3, 4)

        box_misc_editor.attach(self.check_editor_lines, 0, 2, 0, 1)
        box_misc_editor.attach(gtk.HSeparator(), 0, 2, 1, 2)
        box_misc_editor.attach(label_editor_colorscheme, 0, 1, 2, 3)
        box_misc_editor.attach(self.combo_editor_colorscheme, 1, 2, 2, 3)
        box_misc_editor.attach(gtk.HSeparator(), 0, 2, 3, 4)
        box_misc_editor.attach(label_font, 0, 1, 4, 5)
        box_misc_editor.attach(self.file_font, 1, 2, 4, 5)

        frame_viewer.add(box_misc_viewer)
        frame_editor.add(box_misc_editor)

        box_misc.pack_start(frame_viewer, False)
        box_misc.pack_start(frame_editor, False)

        # Shortcuts
        box_shortcuts.pack_start(self.label_shortcuts_explanation, False)
        box_shortcuts.pack_start(scroll_shortcuts)

        # Consoles
        buttons_consoles.pack_start(self.button_consoles_append)
        buttons_consoles.pack_start(self.button_consoles_rename)
        buttons_consoles.pack_start(self.button_consoles_remove)

        box_consoles.pack_start(buttons_consoles, False)
        box_consoles.pack_start(scroll_consoles, True)

        # Emulators
        buttons_emulators.pack_start(self.button_emulators_append)
        buttons_emulators.pack_start(self.button_emulators_rename)
        buttons_emulators.pack_start(self.button_emulators_remove)

        box_emulators.pack_start(buttons_emulators, False)
        box_emulators.pack_start(scroll_emulators, True)


    def __init_signals(self):
        """
        Initialize widgets signals
        """

        # Viewer
        self.entry_arguments.connect("icon-press", on_entry_clear)

        # Shortcuts
        self.label_shortcuts_explanation.connect("size-allocate",
            set_size_request)
        self.cell_shortcuts_keys.connect("accel-edited", self.__edit_keys)
        self.cell_shortcuts_keys.connect("accel-cleared", self.__clear_keys)

        # Consoles
        self.button_consoles_append.connect("clicked", self.item_append)
        self.button_consoles_rename.connect("clicked", self.item_rename)
        self.button_consoles_remove.connect("clicked", self.item_remove)

        self.treeview_consoles.connect("button-press-event", self.item_modify)
        self.treeview_consoles.connect("key-release-event", self.item_modify)

        # Emulators
        self.button_emulators_append.connect("clicked", self.item_append)
        self.button_emulators_rename.connect("clicked", self.item_rename)
        self.button_emulators_remove.connect("clicked", self.item_remove)

        self.treeview_emulators.connect("button-press-event", self.item_modify)
        self.treeview_emulators.connect("key-release-event", self.item_modify)


    def __start_interface(self):
        """
        Load data and start interface
        """

        self.load_configuration()

        self.show_all()
        self.set_infobar()

        self.box_tab_interface.show_all()
        self.box_tab_misc.show_all()
        self.box_tab_shortcuts.show_all()
        self.box_tab_emulators.show_all()
        self.box_tab_consoles.show_all()

        self.button_emulators_rename.set_sensitive(False)
        self.button_emulators_remove.set_sensitive(False)
        self.button_consoles_rename.set_sensitive(False)
        self.button_consoles_remove.set_sensitive(False)

        self.__stop_interface(self.run())


    def __stop_interface(self, response):
        """
        Save data and stop interface
        """

        if response == gtk.RESPONSE_ACCEPT:
            self.config.modify("gem", "load_console_startup",
                int(self.check_load_console.get_active()))

            self.config.modify("gem", "show_output",
                int(self.check_output.get_active()))
            self.config.modify("gem", "show_statusbar",
                int(self.check_statusbar.get_active()))

            self.config.modify("columns", "play",
                int(self.check_column_play.get_active()))
            self.config.modify("columns", "last_play",
                int(self.check_column_last_play.get_active()))
            self.config.modify("columns", "play_time",
                int(self.check_column_play_time.get_active()))
            self.config.modify("columns", "installed",
                int(self.check_column_installed.get_active()))
            self.config.modify("columns", "flags",
                int(self.check_column_flags.get_active()))

            self.config.modify("gem", "games_treeview_lines",
                self.lines[self.combo_lines.get_active_text()])

            self.config.modify("viewer", "native",
                int(self.check_viewer.get_active()))
            self.config.modify("viewer", "binary",
                self.file_viewer.get_filename())
            self.config.modify("viewer", "options",
                self.entry_arguments.get_text())

            self.config.modify("editor", "lines",
                int(self.check_editor_lines.get_active()))
            self.config.modify("editor", "colorscheme",
                self.combo_editor_colorscheme.get_active_text())
            self.config.modify("editor", "font",
                self.file_font.get_font_name())

            for text, value, option in self.model_shortcuts:
                self.config.modify("keys", option, value)

            self.config.update()

            self.need_reload = True

        if self.interface is not None and self.need_reload:
            self.interface.load_interface()

        self.destroy()


    def load_configuration(self):
        """
        Load configuration files and fill widgets
        """

        # ------------------------------------
        #   Interface
        # ------------------------------------

        self.check_load_console.set_active(
            bool(int(self.config.item("gem", "load_console_startup", 1))))

        self.check_output.set_active(
            bool(int(self.config.item("gem", "show_output", 1))))

        self.check_statusbar.set_active(
            bool(int(self.config.item("gem", "show_statusbar", 1))))

        self.check_column_play.set_active(
            bool(int(self.config.item("columns", "play", 1))))

        self.check_column_last_play.set_active(
            bool(int(self.config.item("columns", "last_play", 1))))

        self.check_column_play_time.set_active(
            bool(int(self.config.item("columns", "play_time", 1))))

        self.check_column_installed.set_active(
            bool(int(self.config.item("columns", "installed", 1))))

        self.check_column_flags.set_active(
            bool(int(self.config.item("columns", "flags", 1))))

        item = None
        for key, value in self.lines.items():
            row = self.model_lines.append([key])

            if self.config.item("gem", "games_treeview_lines", "none") == value:
                item = row

        if item is not None:
            self.combo_lines.set_active_iter(item)

        # ------------------------------------
        #   Viewer
        # ------------------------------------

        self.check_viewer.set_active(
            bool(int(self.config.item("viewer", "native", 1))))

        self.file_viewer.set_filename(self.config.item("viewer", "binary"))
        self.entry_arguments.set_text(self.config.item("viewer", "options"))

        # ------------------------------------
        #   Editor
        # ------------------------------------

        self.check_editor_lines.set_active(
            bool(int(self.config.item("editor", "lines", 1))))

        style_manager = gtksourceview2.StyleSchemeManager()

        colorscheme = self.config.item("editor", "colorscheme", "classic")

        item = None
        for path in style_manager.get_search_path():
            for element in glob(path_join(path, "*.xml")):
                name = splitext(basename(element))[0]

                row = self.model_editor_colorscheme.append([name])
                if name == colorscheme:
                    item = row

        if item is not None:
            self.combo_editor_colorscheme.set_active_iter(item)

        self.file_font.set_font_name(
            self.config.item("editor", "font", "Sans 12"))

        # ------------------------------------
        #   Shortcuts
        # ------------------------------------

        for option, (string, default) in self.shortcuts.items():
            value = self.config.item("keys", option, default)

            self.model_shortcuts.append([string, value, option])

        # ------------------------------------
        #   Consoles
        # ------------------------------------

        for name in self.consoles.sections():
            image = icon_from_data(self.consoles.item(name, "icon"), self.empty)

            self.model_consoles.append(
                [image, name, self.consoles.item(name, "emulator")])

        # ------------------------------------
        #   Emulators
        # ------------------------------------

        for name in self.emulators.sections():
            image = icon_from_data(
                self.emulators.item(name, "icon"), self.empty)

            self.model_emulators.append(
                [image, name, self.emulators.item(name, "binary")])


    def __edit_keys(self, widget, path, key, mods, hwcode):
        """
        Edit a shortcut
        """

        if gtk.accelerator_valid(key, mods):
            self.model_shortcuts.set_value(self.model_shortcuts.get_iter(path),
                1, gtk.accelerator_name(key, mods))


    def __clear_keys(self, widget, path):
        """
        Clear a shortcut
        """

        self.model_shortcuts.set_value(
            self.model_shortcuts.get_iter(path), 1, None)


    def item_append(self, widget):
        """
        Append a new item in the treeview
        """

        # ------------------------------------
        #   Consoles
        # ------------------------------------

        if widget == self.button_consoles_append:
            dialog = ConsoleDialog(
                "%s - %s" % (self.title, _("New console")), self)
            name, data = dialog.start()

            if data is not None:
                for (option, value) in data.items():
                    if value is None:
                        value = str()

                    if len(str(value)) > 0:
                        self.consoles.modify(name, option, str(value))

                self.consoles.update()

                self.model_consoles.append([icon_from_data(
                    self.consoles.item(name, "icon"), self.empty),
                    name, self.consoles.item(name, "emulator")])

                self.need_reload = True

        # ------------------------------------
        #   Emulators
        # ------------------------------------

        if widget == self.button_emulators_append:
            dialog = EmulatorDialog(
                "%s - %s" % (self.title, _("New emulator")), self)
            name, data = dialog.start()

            if data is not None:
                for (option, value) in data.items():
                    if value is None:
                        value = str()

                    if len(str(value)) > 0:
                        self.emulators.modify(name, option, str(value))

                self.emulators.update()

                self.model_emulators.append([icon_from_data(
                    self.emulators.item(name, "icon"), self.empty),
                    name, self.emulators.item(name, "binary")])

                self.need_reload = True


    def item_modify(self, treeview, event):
        """
        Modify selected item
        """

        name, modify, result = None, None, None

        # Keyboard
        if event.type == gtk.gdk.KEY_RELEASE:

            model, treeiter = treeview.get_selection().get_selected()
            if treeiter is not None:
                name = model.get_value(treeiter, 1)

                if treeview == self.treeview_consoles:
                    self.button_consoles_rename.set_sensitive(True)
                    self.button_consoles_remove.set_sensitive(True)
                elif treeview == self.treeview_emulators:
                    self.button_emulators_rename.set_sensitive(True)
                    self.button_emulators_remove.set_sensitive(True)

                if gtk.gdk.keyval_name(event.keyval) == "Return":
                    modify = True

        # Mouse
        elif (event.type in [gtk.gdk.BUTTON_PRESS, gtk.gdk._2BUTTON_PRESS]) \
            and (event.button == 1 or event.button == 3):

            selection = treeview.get_path_at_pos(int(event.x), int(event.y))
            if selection is not None:
                model = treeview.get_model()

                treeiter = model.get_iter(selection[0])
                name = model.get_value(treeiter, 1)

                if treeview == self.treeview_consoles:
                    self.button_consoles_rename.set_sensitive(True)
                    self.button_consoles_remove.set_sensitive(True)
                elif treeview == self.treeview_emulators:
                    self.button_emulators_rename.set_sensitive(True)
                    self.button_emulators_remove.set_sensitive(True)

                if event.button == 1 and event.type == gtk.gdk._2BUTTON_PRESS:
                    modify = True

        # ----------------------------
        #   Game selected
        # ----------------------------

        if name is not None and modify:
            title = "%s - %s" % (self.title, name)

            # ------------------------------------
            #   Consoles
            # ------------------------------------

            if treeview == self.treeview_consoles:
                dialog = ConsoleDialog(title, name, self)
                name, data = dialog.start()

                if data is not None:
                    for (option, value) in data.items():
                        if value is None:
                            value = str()

                        if len(str(value)) > 0:
                            self.consoles.modify(name, option, str(value))

                    self.consoles.update()

                    self.model_consoles[treeiter][0] = icon_from_data(
                        self.consoles.item(name, "icon"), self.empty)
                    self.model_consoles[treeiter][1] = name
                    self.model_consoles[treeiter][2] = \
                        self.consoles.item(name, "emulator")

                    self.need_reload = True

            # ------------------------------------
            #   Emulators
            # ------------------------------------

            if treeview == self.treeview_emulators:
                dialog = EmulatorDialog(title, name, self)
                name, data = dialog.start()

                if data is not None:
                    for (option, value) in data.items():
                        if value is None:
                            value = str()

                        if len(str(value)) > 0:
                            self.emulators.modify(name, option, str(value))

                    self.emulators.update()

                    self.model_emulators[treeiter][0] = icon_from_data(
                        self.emulators.item(name, "icon"), self.empty)
                    self.model_emulators[treeiter][1] = name
                    self.model_emulators[treeiter][2] = \
                        self.emulators.item(name, "binary")

                    self.need_reload = True


    def item_rename(self, widget):
        """
        Rename selected item
        """

        name = None

        if widget == self.button_consoles_rename:
            config, treeview = self.consoles, self.treeview_consoles
        elif widget == self.button_emulators_rename:
            config, treeview = self.emulators, self.treeview_emulators

        model, treeiter = treeview.get_selection().get_selected()
        if treeiter is not None:
            name = model.get_value(treeiter, 1)

        # ----------------------------
        #   Game selected
        # ----------------------------

        if name is not None:
            dialog = Dialog.Input(self, _("Rename %s" % name),
                _("Set a new name"), name, config.sections())

            if dialog.run() == gtk.RESPONSE_OK and \
                not dialog.entry.get_text() == name:
                config.rename(name, dialog.entry.get_text())
                config.update()

                model[treeiter][1] = dialog.entry.get_text()

                self.need_reload = True

            dialog.destroy()


    def item_remove(self, widget):
        """
        Remove an item in the treeview
        """

        name = None

        if widget == self.button_consoles_remove:
            config, treeview = self.consoles, self.treeview_consoles
        elif widget == self.button_emulators_remove:
            config, treeview = self.emulators, self.treeview_emulators

        model, treeiter = treeview.get_selection().get_selected()
        if treeiter is not None:
            name = model.get_value(treeiter, 1)

        # ----------------------------
        #   Game selected
        # ----------------------------

        if name is not None:
            dialog = Dialog.Question(self, _("Remove %s" % name),
                _("Would you really want to remove this entry ?"))

            if dialog.run() == gtk.RESPONSE_YES:
                config.remove(name)
                config.update()

                model.remove(treeiter)

                self.need_reload = True

            dialog.destroy()


class ConsoleDialog(Window.Default):

    def __init__(self, title, name=None, parent=None):
        """
        Constructor
        """

        Window.Default.__init__(self, parent, title, 640, 320)

        # ------------------------------------
        #   Variables
        # ------------------------------------

        self.path = None
        self.new_console = True

        if parent is not None:
            self.consoles = parent.consoles
            self.emulators = parent.emulators
        else:
            self.consoles = Configuration(
                expanduser(path_join(Conf.User, "consoles.conf")))
            self.emulators = Configuration(
                expanduser(path_join(Conf.User, "emulators.conf")))

        self.emulators_sections = sorted(self.emulators.sections())

        # ------------------------------------
        #   Initialization
        # ------------------------------------

        # Init widgets
        self.__init_widgets()

        # Init signals
        self.__init_signals()

        # ------------------------------------
        #   Load interface data
        # ------------------------------------

        self.show_all()
        self.set_infobar()

        # Append emulators data into combobox
        for emulator in self.emulators_sections:
            self.model_emulator.append([emulator])

        # Avoid to have apply button available without data
        self.set_response_sensitive(gtk.RESPONSE_ACCEPT, False)

        # Load data for an existing console
        if name is not None and parent is not None:
            self.new_console = False
            self.entry_name.set_sensitive(False)
            self.load_interface(name)


    def __init_widgets(self):
        """
        Initialize interface widgets
        """

        # ------------------------------------
        #   Grid
        # ------------------------------------

        scrollview = gtk.ScrolledWindow()
        view = gtk.Viewport()

        box = gtk.Table()

        # Properties
        scrollview.set_border_width(4)
        scrollview.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        view.set_shadow_type(gtk.SHADOW_NONE)

        box.set_border_width(4)
        box.set_row_spacings(8)
        box.set_col_spacings(10)
        box.set_homogeneous(False)

        # ------------------------------------
        #   Name
        # ------------------------------------

        label_name = gtk.Label()
        self.entry_name = gtk.Entry()

        # Properties
        label_name.set_alignment(0, .5)
        label_name.set_text(_("Name"))

        self.entry_name.set_icon_from_stock(1, gtk.STOCK_CLEAR)

        # ------------------------------------
        #   Folder
        # ------------------------------------

        label_folder = gtk.Label()
        self.file_folder = gtk.FileChooserButton(_("Choose the ROMs folder"))

        # Properties
        label_folder.set_alignment(0, .5)
        label_folder.set_text(_("ROM's folder"))

        self.file_folder.set_action(gtk.FILE_CHOOSER_ACTION_SELECT_FOLDER)

        # ------------------------------------
        #   Icon
        # ------------------------------------

        self.button_icon = gtk.Button()

        self.image_icon = gtk.Image()

        # Properties
        self.button_icon.set_image(self.image_icon)

        self.image_icon.set_from_pixbuf(
            icon_from_data(None, width=64, height=64))

        # ------------------------------------
        #   Extensions
        # ------------------------------------

        label_extensions = gtk.Label()
        self.entry_extensions = gtk.Entry()

        label_extensions_explanation = gtk.Label()

        # Properties
        label_extensions.set_alignment(0, .5)
        label_extensions.set_text(_("ROM's extensions"))

        self.entry_extensions.set_icon_from_stock(1, gtk.STOCK_CLEAR)

        label_extensions_explanation.set_use_markup(True)
        label_extensions_explanation.set_markup("<i>%s</i>" % (
            _("Use ; to separate extensions")))

        # ------------------------------------
        #   Emulator
        # ------------------------------------

        label_emulator = gtk.Label()
        self.combo_emulator = gtk.ComboBox()

        self.model_emulator = gtk.ListStore(str)
        cell_emulator = gtk.CellRendererText()

        # Properties
        label_emulator.set_alignment(0, .5)
        label_emulator.set_text(_("Emulator"))

        self.combo_emulator.set_model(self.model_emulator)
        self.combo_emulator.pack_start(cell_emulator, True)
        self.combo_emulator.add_attribute(cell_emulator, "text", 0)

        # ------------------------------------
        #   Add widgets into interface
        # ------------------------------------

        view.add(box)
        scrollview.add(view)

        box.attach(label_name, 0, 1, 0, 1, gtk.FILL, gtk.FILL)
        box.attach(self.entry_name, 1, 2, 0, 1, yoptions=gtk.FILL)
        box.attach(label_folder, 0, 1, 1, 2, gtk.FILL, gtk.FILL)
        box.attach(self.file_folder, 1, 2, 1, 2, yoptions=gtk.FILL)
        box.attach(self.button_icon, 2, 3, 0, 2, gtk.FILL, gtk.FILL)
        box.attach(label_extensions, 0, 1, 2, 3, gtk.FILL, gtk.FILL)
        box.attach(self.entry_extensions, 1, 3, 2, 3, yoptions=gtk.FILL)
        box.attach(label_extensions_explanation, 1, 3, 3, 4, yoptions=gtk.FILL)
        box.attach(gtk.HSeparator(), 0, 3, 4, 5, yoptions=gtk.FILL)
        box.attach(label_emulator, 0, 1, 5, 6, gtk.FILL, gtk.FILL)
        box.attach(self.combo_emulator, 1, 3, 5, 6, yoptions=gtk.FILL)

        self.get_content_area().pack_start(scrollview, True)


    def __init_signals(self):
        """
        Initialize widgets signals
        """

        self.entry_name.connect("changed", self.check_value)
        self.entry_name.connect("icon-press", on_entry_clear)
        self.entry_extensions.connect("icon-press", on_entry_clear)

        self.button_icon.connect("clicked", self.select_icon)


    def check_value(self, widget, pos=None, event=None):
        """
        Check if a value is not already used
        """

        if self.new_console:
            section = self.consoles.has_section(self.entry_name.get_text())

            if not section:
                self.set_response_sensitive(gtk.RESPONSE_ACCEPT, True)

                self.set_infobar()

            elif section:
                self.set_response_sensitive(gtk.RESPONSE_ACCEPT, False)

                self.set_infobar(_("This console already exist, please, "
                    "choose another name"))


    def load_interface(self, name):
        """
        Insert data into interface's widgets
        """

        # Name
        self.entry_name.set_text(name)

        # Folder
        folder = expanduser(self.consoles.item(name, "roms"))
        if exists(folder):
            self.file_folder.set_current_folder(folder)

        # Extensions
        self.entry_extensions.set_text(self.consoles.item(name, "exts", str()))

        # Icon
        self.path = self.consoles.item(name, "icon")
        self.image_icon.set_from_pixbuf(
            icon_from_data(self.path, self.empty, 64, 64))

        # Emulator
        if self.consoles.item(name, "emulator") in self.emulators_sections:
            self.combo_emulator.set_active(self.emulators_sections.index(
                self.consoles.item(name, "emulator")))

        self.set_response_sensitive(gtk.RESPONSE_ACCEPT, True)


    def save_interface(self):
        """
        Return all the data from interface
        """

        self.data = dict()

        self.section = self.entry_name.get_text()

        path = self.path
        if path is not None and \
            path_join(get_data("icons"), basename(path)) == path:
            path = splitext(basename(path))[0]

        self.data["roms"] = self.file_folder.get_filename()
        self.data["icon"] = path
        self.data["exts"] = self.entry_extensions.get_text()
        self.data["emulator"] = self.combo_emulator.get_active_text()


class EmulatorDialog(Window.Default):

    def __init__(self, title, name=None, parent=None):
        """
        Constructor
        """

        Window.Default.__init__(self, parent, title, 640, 520)

        # ------------------------------------
        #   Variables
        # ------------------------------------

        self.path = None
        self.new_emulator = True

        if parent is not None:
            self.emulators = parent.emulators
        else:
            self.emulators = Configuration(
                expanduser(path_join(Conf.User, "emulators.conf")))

        # ------------------------------------
        #   Initialization
        # ------------------------------------

        # Init widgets
        self.__init_widgets()

        # Init signals
        self.__init_signals()

        # ------------------------------------
        #   Load interface data
        # ------------------------------------

        self.show_all()
        self.set_infobar()

        # Load data for an existing emulator
        if name is not None and parent is not None:
            self.new_emulator = False
            self.entry_name.set_sensitive(False)
            self.load_interface(name)


    def __init_widgets(self):
        """
        Initialize interface widgets
        """

        # ------------------------------------
        #   Grid
        # ------------------------------------

        scrollview = gtk.ScrolledWindow()
        view = gtk.Viewport()

        box = gtk.Table()

        box_regex = gtk.Table()
        box_arguments = gtk.Table()

        # Properties
        scrollview.set_border_width(4)
        scrollview.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        view.set_shadow_type(gtk.SHADOW_NONE)

        box.set_border_width(4)
        box.set_row_spacings(8)
        box.set_col_spacings(8)
        box.set_homogeneous(False)

        box_regex.set_border_width(8)
        box_regex.set_row_spacings(8)
        box_regex.set_col_spacings(8)
        box_regex.set_homogeneous(False)

        box_arguments.set_border_width(8)
        box_arguments.set_row_spacings(8)
        box_arguments.set_col_spacings(8)
        box_arguments.set_homogeneous(False)

        # ------------------------------------
        #   Name
        # ------------------------------------

        label_name = gtk.Label()
        self.entry_name = gtk.Entry()

        # Properties
        label_name.set_alignment(0, .5)
        label_name.set_text(_("Name"))

        self.entry_name.set_icon_from_stock(1, gtk.STOCK_CLEAR)

        # ------------------------------------
        #   Binary
        # ------------------------------------

        label_binary = gtk.Label()
        self.file_binary = gtk.FileChooserButton(_("Choose the emulator"))

        # Properties
        label_binary.set_alignment(0, .5)
        label_binary.set_text(_("Binary"))

        # ------------------------------------
        #   Icon
        # ------------------------------------

        self.button_icon = gtk.Button()

        self.image_icon = gtk.Image()

        # Properties
        self.button_icon.set_image(self.image_icon)

        self.image_icon.set_from_pixbuf(
            icon_from_data(None, width=64, height=64))

        # ------------------------------------
        #   Configuration
        # ------------------------------------

        label_configuration = gtk.Label()
        self.file_configuration = gtk.FileChooserButton(
            _("Choose the configuration file"))

        # Properties
        label_configuration.set_alignment(0, .5)
        label_configuration.set_text(_("Configuration file"))

        # ------------------------------------
        #   Regex
        # ------------------------------------

        frame_regex = gtk.Frame()
        label_regex = gtk.Label()

        label_save = gtk.Label()
        self.entry_save = gtk.Entry()

        label_snapshot = gtk.Label()
        self.entry_snapshot = gtk.Entry()

        label_regex_explanation = gtk.Label()

        # Properties
        frame_regex.set_label_widget(label_regex)

        label_regex.set_padding(4, -1)
        label_regex.set_use_markup(True)
        label_regex.set_markup("<b>%s</b>" % _("Regular expressions for files"))

        label_save.set_alignment(0, .5)
        label_save.set_text(_("Save"))

        self.entry_save.set_icon_from_stock(1, gtk.STOCK_CLEAR)

        label_snapshot.set_alignment(0, .5)
        label_snapshot.set_text(_("Snapshots"))

        self.entry_snapshot.set_icon_from_stock(1, gtk.STOCK_CLEAR)

        label_regex_explanation.set_use_markup(True)
        label_regex_explanation.set_markup("<i>%s</i>" % (
            _("<name> = ROM filename and <lname> = ROM lowercase "
            "filename").replace('>', "&gt;").replace('<', "&lt;")))

        # ------------------------------------
        #   Arguments
        # ------------------------------------

        frame_arguments = gtk.Frame()
        label_arguments = gtk.Label()

        label_default = gtk.Label()
        self.entry_default = gtk.Entry()

        label_windowed = gtk.Label()
        self.entry_windowed = gtk.Entry()

        label_fullscreen = gtk.Label()
        self.entry_fullscreen = gtk.Entry()

        # Properties
        frame_arguments.set_label_widget(label_arguments)

        label_arguments.set_padding(4, -1)
        label_arguments.set_use_markup(True)
        label_arguments.set_markup("<b>%s</b>" % _("Emulator arguments"))

        label_default.set_alignment(0, .5)
        label_default.set_text(_("Default options"))

        self.entry_default.set_icon_from_stock(1, gtk.STOCK_CLEAR)

        label_windowed.set_alignment(0, .5)
        label_windowed.set_text(_("Windowed"))

        self.entry_windowed.set_icon_from_stock(1, gtk.STOCK_CLEAR)

        label_fullscreen.set_alignment(0, .5)
        label_fullscreen.set_text(_("Fullscreen"))

        self.entry_fullscreen.set_icon_from_stock(1, gtk.STOCK_CLEAR)

        # ------------------------------------
        #   Add widgets into interface
        # ------------------------------------

        view.add(box)
        scrollview.add(view)

        box.attach(label_name, 0, 1, 0, 1, gtk.FILL, gtk.FILL)
        box.attach(self.entry_name, 1, 2, 0, 1, yoptions=gtk.FILL)
        box.attach(label_binary, 0, 1, 1, 2, gtk.FILL, gtk.FILL)
        box.attach(self.file_binary, 1, 2, 1, 2, yoptions=gtk.FILL)
        box.attach(self.button_icon, 2, 3, 0, 2, gtk.FILL, gtk.FILL)
        box.attach(label_configuration, 0, 1, 2, 3, gtk.FILL, gtk.FILL)
        box.attach(self.file_configuration, 1, 3, 2, 3, yoptions=gtk.FILL)
        box.attach(gtk.HSeparator(), 0, 3, 3, 4, yoptions=gtk.FILL)
        box.attach(frame_regex, 0, 3, 4, 5, gtk.FILL, gtk.FILL)
        box.attach(frame_arguments, 0, 3, 5, 6, gtk.FILL, gtk.FILL)

        box_regex.attach(label_save, 0, 1, 0, 1, gtk.FILL, gtk.FILL)
        box_regex.attach(self.entry_save, 1, 2, 0, 1, yoptions=gtk.FILL)
        box_regex.attach(label_snapshot, 0, 1, 1, 2, gtk.FILL, gtk.FILL)
        box_regex.attach(self.entry_snapshot, 1, 2, 1, 2, yoptions=gtk.FILL)
        box_regex.attach(label_regex_explanation, 0, 2, 2, 3, yoptions=gtk.FILL)

        frame_regex.add(box_regex)

        box_arguments.attach(label_default, 0, 1, 0, 1, gtk.FILL, gtk.FILL)
        box_arguments.attach(self.entry_default, 1, 2, 0, 1,
            yoptions=gtk.FILL)
        box_arguments.attach(label_windowed, 0, 1, 1, 2, gtk.FILL, gtk.FILL)
        box_arguments.attach(self.entry_windowed, 1, 2, 1, 2,
            yoptions=gtk.FILL)
        box_arguments.attach(label_fullscreen, 0, 1, 2, 3, gtk.FILL, gtk.FILL)
        box_arguments.attach(self.entry_fullscreen, 1, 2, 2, 3,
            yoptions=gtk.FILL)

        frame_arguments.add(box_arguments)

        self.get_content_area().pack_start(scrollview, True)


    def __init_signals(self):
        """
        Initialize widgets signals
        """

        self.entry_name.connect("changed", self.check_value)
        self.entry_name.connect("icon-press", on_entry_clear)
        self.entry_save.connect("icon-press", on_entry_clear)
        self.entry_snapshot.connect("icon-press", on_entry_clear)
        self.entry_default.connect("icon-press", on_entry_clear)
        self.entry_windowed.connect("icon-press", on_entry_clear)
        self.entry_fullscreen.connect("icon-press", on_entry_clear)

        self.button_icon.connect("clicked", self.select_icon)


    def check_value(self, widget, pos=None, event=None):
        """
        Check if a value is not already used
        """

        if self.new_emulator:
            section = self.emulators.has_section(self.entry_name.get_text())

            if not section:
                self.set_response_sensitive(gtk.RESPONSE_ACCEPT, True)

                self.set_infobar()

            elif section:
                self.set_response_sensitive(gtk.RESPONSE_ACCEPT, False)

                self.set_infobar(_("This emulator already exist, please, "
                    "choose another name"))


    def load_interface(self, name):
        """
        Insert data into interface's widgets
        """

        # Name
        self.entry_name.set_text(name)

        # Binary
        binary = expanduser(self.emulators.item(name, "binary"))
        if exists(binary):
            self.file_binary.set_filename(binary)

        # Configuration
        configuration = expanduser(
            self.emulators.item(name, "configuration", str()))
        if exists(configuration):
            self.file_configuration.set_filename(configuration)

        # Icon
        self.path = self.emulators.item(name, "icon")
        self.image_icon.set_from_pixbuf(
            icon_from_data(self.path, self.empty, 64, 64))

        # Regex
        self.entry_save.set_text(
            self.emulators.item(name, "save", str()))
        self.entry_snapshot.set_text(
            self.emulators.item(name, "snaps", str()))

        # Arguments
        self.entry_default.set_text(
            self.emulators.item(name, "default", str()))
        self.entry_windowed.set_text(
            self.emulators.item(name, "windowed", str()))
        self.entry_fullscreen.set_text(
            self.emulators.item(name, "fullscreen", str()))


    def save_interface(self):
        """
        Return all the data from interface
        """

        self.data = dict()

        self.section = self.entry_name.get_text()

        path = self.path
        if path is not None \
            and path_join(get_data("icons"), basename(path)) == path:
            path = splitext(basename(path))[0]

        self.data["binary"] = self.file_binary.get_filename()
        self.data["configuration"] = self.file_configuration.get_filename()
        self.data["icon"] = path
        self.data["save"] = self.entry_save.get_text()
        self.data["snaps"] = self.entry_snapshot.get_text()
        self.data["default"] = self.entry_default.get_text()
        self.data["windowed"] = self.entry_windowed.get_text()
        self.data["fullscreen"] = self.entry_fullscreen.get_text()
