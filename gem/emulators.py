# -*- coding: utf-8 -*-

# ------------------------------------------------------------------
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ------------------------------------------------------------------

# ------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------

# System
from os.path import expanduser

from subprocess import Popen
from shlex import split as shlex_split

# Interface
import gtk

from utils import get_data

# Translation
from gettext import gettext as _
from gettext import textdomain
from gettext import bindtextdomain

# Regex
from re import match

# ------------------------------------------------------------------
#   Translation
# ------------------------------------------------------------------

bindtextdomain("gem", get_data("i18n"))
textdomain("gem")

# ------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------

class Mednafen(gtk.ScrolledWindow):
    """
    http://mednafen.sourceforge.net/documentation/
    """

    def __init__(self, console, configuration_file=None):
        """
        Constructor
        """

        gtk.ScrolledWindow.__init__(self)

        # ------------------------------------
        #   Variables
        # ------------------------------------

        self.description = "Mednafen"

        self.console = console
        if self.console == "gbc":
            self.console = "gb"

        if configuration_file is not None:
            self.configuration = configuration_file
        else:
            self.configuration = expanduser("~/.mednafen/mednafen-09x.cfg")

        self.data = dict()

        self.pixshaders = ["none", "autoip", "autoipsharper", "scale2x",
            "sabr", "ipsharper", "ipxnoty", "ipynotx", "ipxnotysharper",
            "ipynotxsharper"]

        self.special = ["none", "hq2x", "hq3x", "hq4x", "scale2x", "scale3x",
            "scale4x", "2xsai", "super2xsai", "supereagle", "nn2x", "nn3x",
            "nn4x", "nny2x", "nny3x", "nny4x"]

        self.binilear = ["0", "1", "x", "y"]

        self.stretch = ["0", "full", "aspect", "aspect_int", "aspect_mult2"]

        self.drivers = ["default", "alsa", "oss", "wasapish", "dsound",
            "wasapi", "sdl", "jack"]

        self.mono_consoles = ["gb", "gba", "gg", "lynx", "md", "ngp", "pce",
            "pcfx", "psx", "sms", "snes", "vb", "wswan"]

        # ------------------------------------
        #   Initialization
        # ------------------------------------

        # Init widgets
        self.__init_widgets()

        # Init signals
        self.__init_signals()


    def __init_widgets(self):
        """
        Initialize interface widgets
        """

        # ------------------------------------
        #   Interface
        # ------------------------------------

        self.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        self.set_border_width(4)

        # ------------------------------------
        #   Grid
        # ------------------------------------

        box = gtk.VBox()

        view = gtk.Viewport()

        # Properties
        view.set_shadow_type(gtk.SHADOW_NONE)

        # ------------------------------------
        #   Sound
        # ------------------------------------

        frame_sound = gtk.Frame()
        label_sound = gtk.Label()

        grid_sound = gtk.Table()

        self.check_sound = gtk.CheckButton()

        label_sound_driver = gtk.Label()
        self.model_sound_driver = gtk.ListStore(str)
        self.combo_sound_driver = gtk.ComboBox()
        cell_sound_driver = gtk.CellRendererText()

        label_sound_device = gtk.Label()
        self.entry_sound_device = gtk.Entry()

        label_sound_volume = gtk.Label()
        self.adjustment_sound_volume = gtk.Adjustment()
        self.scale_sound_volume = gtk.HScale()

        # Properties
        frame_sound.set_label_widget(label_sound)
        frame_sound.set_border_width(4)

        label_sound.set_padding(4, -1)
        label_sound.set_use_markup(True)
        label_sound.set_markup("<b>%s</b>" % _("Sound"))

        grid_sound.set_border_width(8)
        grid_sound.set_col_spacings(8)
        grid_sound.set_row_spacings(8)

        self.check_sound.set_label(_("Enable sound output"))

        label_sound_driver.set_alignment(0, .5)
        label_sound_driver.set_use_markup(True)
        label_sound_driver.set_markup("%s:" % _("Select sound driver"))

        self.combo_sound_driver.set_model(self.model_sound_driver)
        self.combo_sound_driver.pack_start(cell_sound_driver, True)
        self.combo_sound_driver.add_attribute(cell_sound_driver, "text", 0)

        label_sound_device.set_alignment(0, .5)
        label_sound_device.set_use_markup(True)
        label_sound_device.set_markup("%s:" % _("Select sound output device"))

        label_sound_volume.set_alignment(0, .5)
        label_sound_volume.set_use_markup(True)
        label_sound_volume.set_markup("%s:" % _("Sound volume"))

        self.adjustment_sound_volume.set_all(0, 0, 150, 1, 1)

        self.scale_sound_volume.set_value_pos(gtk.POS_LEFT)
        self.scale_sound_volume.set_adjustment(
            self.adjustment_sound_volume)

        grid_sound.attach(self.check_sound, 0, 3, 0, 1)
        grid_sound.attach(gtk.HSeparator(), 0, 3, 1, 2)
        grid_sound.attach(label_sound_driver, 0, 1, 2, 3)
        grid_sound.attach(self.combo_sound_driver, 2, 3, 2, 3)
        grid_sound.attach(label_sound_device, 0, 1, 3, 4)
        grid_sound.attach(self.entry_sound_device, 2, 3, 3, 4)
        grid_sound.attach(gtk.HSeparator(), 0, 3, 4, 5)
        grid_sound.attach(label_sound_volume, 0, 1, 5, 6)
        grid_sound.attach(self.scale_sound_volume, 1, 3, 5, 6)

        if self.console in self.mono_consoles or self.console == "nes":
            grid_sound.attach(gtk.HSeparator(), 0, 3, 6, 7)

        if self.console in self.mono_consoles:
            self.check_forcemono = gtk.CheckButton()

            # Properties
            self.check_forcemono.set_label(
                _("Force monophonic sound output"))

            grid_sound.attach(self.check_forcemono, 0, 1, 7, 8)

        if self.console == "nes":
            self.check_less_accurate = gtk.CheckButton()

            label_rate_tolerance = gtk.Label()
            adjustment_rate_tolerance = gtk.Adjustment()
            self.spin_rate_tolerance = gtk.SpinButton()

            label_sound_quality = gtk.Label()
            self.adjustment_sound_quality = gtk.Adjustment()
            self.scale_sound_quality = gtk.HScale()

            # Properties
            self.check_less_accurate.set_label(
                _("Enable less-accurate, Namco 106 sound emulation"))

            label_rate_tolerance.set_alignment(0, .5)
            label_rate_tolerance.set_use_markup(True)
            label_rate_tolerance.set_markup("%s:" % (
                _("Output rate tolerance")))

            adjustment_rate_tolerance.set_all(
                .00004, .0000001, .01, .0001, .001)

            self.spin_rate_tolerance.set_digits(7)
            self.spin_rate_tolerance.set_adjustment(
                adjustment_rate_tolerance)

            label_sound_quality.set_alignment(0, .5)
            label_sound_quality.set_use_markup(True)
            label_sound_quality.set_markup("%s:" % _("Sound quality"))

            self.adjustment_sound_quality.set_all(0, -2, 3, 1, 1)

            self.scale_sound_quality.set_value_pos(gtk.POS_LEFT)
            self.scale_sound_quality.set_adjustment(
                self.adjustment_sound_quality)

            grid_sound.attach(self.check_less_accurate, 0, 3, 8, 9)
            grid_sound.attach(label_rate_tolerance, 0, 1, 9, 10)
            grid_sound.attach(self.spin_rate_tolerance, 2, 3, 9, 10,
                xoptions=gtk.FILL)
            grid_sound.attach(label_sound_quality, 0, 1, 10, 11)
            grid_sound.attach(self.scale_sound_quality, 1, 3, 10, 11)

        frame_sound.add(grid_sound)

        box.pack_start(frame_sound, False, False)

        # ------------------------------------
        #   Video
        # ------------------------------------

        frame_video = gtk.Frame()
        label_video = gtk.Label()

        grid_video = gtk.Table()

        label_resolution = gtk.Label()
        adjustment_resolution_x = gtk.Adjustment()
        self.spin_resolution_x = gtk.SpinButton()
        adjustment_resolution_y = gtk.Adjustment()
        self.spin_resolution_y = gtk.SpinButton()

        label_scaling = gtk.Label()
        adjustment_scaling_x = gtk.Adjustment()
        self.spin_scaling_x = gtk.SpinButton()
        adjustment_scaling_y = gtk.Adjustment()
        self.spin_scaling_y = gtk.SpinButton()

        label_scaling_fs = gtk.Label()
        adjustment_scaling_fs_x = gtk.Adjustment()
        self.spin_scaling_fs_x = gtk.SpinButton()
        adjustment_scaling_fs_y = gtk.Adjustment()
        self.spin_scaling_fs_y = gtk.SpinButton()

        # Properties
        frame_video.set_label_widget(label_video)
        frame_video.set_border_width(4)

        label_video.set_padding(4, -1)
        label_video.set_use_markup(True)
        label_video.set_markup("<b>%s</b>" % _("Video"))

        label_resolution.set_alignment(0, .5)
        label_resolution.set_use_markup(True)
        label_resolution.set_markup("%s:" % _("Resolution"))

        adjustment_resolution_x.set_all(1, 0, 65536, 10, 50)
        adjustment_resolution_y.set_all(1, 0, 65536, 10, 50)

        self.spin_resolution_x.set_adjustment(adjustment_resolution_x)
        self.spin_resolution_y.set_adjustment(adjustment_resolution_y)

        label_scaling.set_alignment(0, .5)
        label_scaling.set_use_markup(True)
        label_scaling.set_markup("%s:" % _("Scaling factor (windowed)"))

        adjustment_scaling_x.set_all(1, .01, 256, .1, .5)
        adjustment_scaling_y.set_all(1, .01, 256, .1, .5)

        self.spin_scaling_x.set_digits(2)
        self.spin_scaling_x.set_adjustment(adjustment_scaling_x)
        self.spin_scaling_y.set_digits(2)
        self.spin_scaling_y.set_adjustment(adjustment_scaling_y)

        label_scaling_fs.set_alignment(0, .5)
        label_scaling_fs.set_use_markup(True)
        label_scaling_fs.set_markup("%s:" % _("Scaling factor (fullscreen)"))

        adjustment_scaling_fs_x.set_all(1, .01, 256, .1, .5)
        adjustment_scaling_fs_y.set_all(1, .01, 256, .1, .5)

        self.spin_scaling_fs_x.set_digits(2)
        self.spin_scaling_fs_x.set_adjustment(adjustment_scaling_fs_x)
        self.spin_scaling_fs_y.set_digits(2)
        self.spin_scaling_fs_y.set_adjustment(adjustment_scaling_fs_y)

        grid_video.set_border_width(8)
        grid_video.set_col_spacings(4)
        grid_video.set_row_spacings(4)

        # ------------------------------------
        #   Filters
        # ------------------------------------

        frame_filter = gtk.Frame()
        label_filter = gtk.Label()

        grid_filter = gtk.Table()

        label_binilear = gtk.Label()
        self.model_binilear = gtk.ListStore(str)
        self.combo_binilear = gtk.ComboBox()
        cell_binilear = gtk.CellRendererText()

        label_stretch = gtk.Label()
        self.model_stretch = gtk.ListStore(str)
        self.combo_stretch = gtk.ComboBox()
        cell_stretch = gtk.CellRendererText()

        label_special = gtk.Label()
        self.model_special = gtk.ListStore(str)
        self.combo_special = gtk.ComboBox()
        cell_special = gtk.CellRendererText()

        label_shader = gtk.Label()
        self.model_shader = gtk.ListStore(str)
        self.combo_shader = gtk.ComboBox()
        cell_shader = gtk.CellRendererText()

        label_scanlines = gtk.Label()
        self.adjustment_scanlines = gtk.Adjustment()
        self.scale_scanlines = gtk.HScale()

        # Properties
        frame_filter.set_label_widget(label_filter)
        frame_filter.set_border_width(4)

        label_filter.set_padding(4, -1)
        label_filter.set_use_markup(True)
        label_filter.set_markup("<b>%s</b>" % _("Filters"))

        label_binilear.set_alignment(0, .5)
        label_binilear.set_use_markup(True)
        label_binilear.set_markup("%s:" % _("Bilinear interpolation"))

        self.combo_binilear.set_model(self.model_binilear)
        self.combo_binilear.pack_start(cell_binilear, True)
        self.combo_binilear.add_attribute(cell_binilear, "text", 0)

        label_stretch.set_alignment(0, .5)
        label_stretch.set_use_markup(True)
        label_stretch.set_markup("%s:" % _("Stretch to fill screen"))

        self.combo_stretch.set_model(self.model_stretch)
        self.combo_stretch.pack_start(cell_stretch, True)
        self.combo_stretch.add_attribute(cell_stretch, "text", 0)

        label_special.set_alignment(0, .5)
        label_special.set_use_markup(True)
        label_special.set_markup("%s:" % _("Special video scaler"))

        self.combo_special.set_model(self.model_special)
        self.combo_special.pack_start(cell_special, True)
        self.combo_special.add_attribute(cell_special, "text", 0)

        label_shader.set_alignment(0, .5)
        label_shader.set_use_markup(True)
        label_shader.set_markup("%s:" % _("OpenGL pixel shader"))

        self.combo_shader.set_model(self.model_shader)
        self.combo_shader.pack_start(cell_shader, True)
        self.combo_shader.add_attribute(cell_shader, "text", 0)

        label_scanlines.set_alignment(0, .5)
        label_scanlines.set_use_markup(True)
        label_scanlines.set_markup("%s:" % _("Scanlines opacity"))

        self.adjustment_scanlines.set_all(0, 0, 100, 1, 5)

        self.scale_scanlines.set_value_pos(gtk.POS_LEFT)
        self.scale_scanlines.set_adjustment(self.adjustment_scanlines)

        grid_filter.set_border_width(8)
        grid_filter.set_col_spacings(4)
        grid_filter.set_row_spacings(4)

        # ------------------------------------
        #   Blur
        # ------------------------------------

        frame_blur = gtk.Frame()
        label_blur = gtk.Label()

        grid_blur = gtk.Table()

        self.check_temporal_blur = gtk.CheckButton()
        self.check_temporal_blur_accumulate = gtk.CheckButton()
        self.label_temporal_blur_amount = gtk.Label()
        adjustment_temporal_blur_amount = gtk.Adjustment()
        self.spin_temporal_blur_amount = gtk.SpinButton()

        # Properties
        frame_blur.set_label_widget(label_blur)
        frame_blur.set_border_width(4)

        label_blur.set_padding(4, -1)
        label_blur.set_use_markup(True)
        label_blur.set_markup("<b>%s</b>" % "Blur")

        self.check_temporal_blur.set_label(_("Enable video temporal blur"))
        self.check_temporal_blur_accumulate.set_label(
            _("Accumulate color data rather than discarding it"))

        self.label_temporal_blur_amount.set_alignment(0, .5)
        self.label_temporal_blur_amount.set_use_markup(True)
        self.label_temporal_blur_amount.set_markup("%s:" % (
            _("Blur amount in accumulation mode")))

        adjustment_temporal_blur_amount.set_all(0, 0, 100, 1, 5)

        self.spin_temporal_blur_amount.set_adjustment(
            adjustment_temporal_blur_amount)

        grid_blur.set_border_width(8)
        grid_blur.set_col_spacings(4)
        grid_blur.set_row_spacings(4)

        # ------------------------------------
        #   Add widgets into interface
        # ------------------------------------

        box.pack_start(frame_video, False, False)
        box.pack_start(frame_filter, False, False)
        box.pack_start(frame_blur, False, False)

        frame_video.add(grid_video)

        grid_video.attach(label_resolution, 0, 1, 0, 1)
        grid_video.attach(self.spin_resolution_x, 1, 2, 0, 1,
            xoptions=gtk.FILL)
        grid_video.attach(gtk.Label('x'), 2, 3, 0, 1, xoptions=gtk.FILL)
        grid_video.attach(self.spin_resolution_y, 3, 4, 0, 1,
            xoptions=gtk.FILL)
        grid_video.attach(label_scaling, 0, 1, 1, 2)
        grid_video.attach(self.spin_scaling_x, 1, 2, 1, 2, xoptions=gtk.FILL)
        grid_video.attach(gtk.Label('x'), 2, 3, 1, 2, xoptions=gtk.FILL)
        grid_video.attach(self.spin_scaling_y, 3, 4, 1, 2, xoptions=gtk.FILL)
        grid_video.attach(label_scaling_fs, 0, 1, 2, 3)
        grid_video.attach(self.spin_scaling_fs_x, 1, 2, 2, 3,
            xoptions=gtk.FILL)
        grid_video.attach(gtk.Label('x'), 2, 3, 2, 3, xoptions=gtk.FILL)
        grid_video.attach(self.spin_scaling_fs_y, 3, 4, 2, 3,
            xoptions=gtk.FILL)

        frame_filter.add(grid_filter)

        grid_filter.attach(label_binilear, 0, 1, 0, 1)
        grid_filter.attach(self.combo_binilear, 2, 3, 0, 1, xoptions=gtk.FILL)
        grid_filter.attach(label_stretch, 0, 1, 1, 2)
        grid_filter.attach(self.combo_stretch, 2, 3, 1, 2, xoptions=gtk.FILL)
        grid_filter.attach(label_special, 0, 1, 2, 3)
        grid_filter.attach(self.combo_special, 2, 3, 2, 3, xoptions=gtk.FILL)
        grid_filter.attach(label_shader, 0, 1, 3, 4)
        grid_filter.attach(self.combo_shader, 2, 3, 3, 4, xoptions=gtk.FILL)
        grid_filter.attach(gtk.HSeparator(), 0, 3, 4, 5, ypadding=8)
        grid_filter.attach(label_scanlines, 0, 1, 5, 6)
        grid_filter.attach(self.scale_scanlines, 1, 3, 5, 6)

        frame_blur.add(grid_blur)

        grid_blur.attach(self.check_temporal_blur, 0, 2, 0, 1)
        grid_blur.attach(self.check_temporal_blur_accumulate, 0, 2, 1, 2)
        grid_blur.attach(self.label_temporal_blur_amount, 0, 1, 2, 3)
        grid_blur.attach(self.spin_temporal_blur_amount, 1, 2, 2, 3,
            xoptions=gtk.FILL)

        view.add(box)
        self.add(view)

        self.show_all()


    def __init_signals(self):
        """
        Initialize widgets signals
        """

        self.check_temporal_blur.connect("toggled", self.__set_sensitive)
        self.check_temporal_blur_accumulate.connect("toggled",
            self.__set_sensitive)


    def __set_sensitive(self, widget=None):
        """
        Set sensitive status for specific widgets
        """

        if self.check_temporal_blur.get_active():
            self.check_temporal_blur_accumulate.set_sensitive(True)

            if self.check_temporal_blur_accumulate.get_active():
                self.label_temporal_blur_amount.set_sensitive(True)
                self.spin_temporal_blur_amount.set_sensitive(True)

        else:
            self.check_temporal_blur_accumulate.set_sensitive(False)
            self.label_temporal_blur_amount.set_sensitive(False)
            self.spin_temporal_blur_amount.set_sensitive(False)


    def index(self, combo, store, value, default):
        """
        Get correct index for a specific value in a store list
        """

        data = self.option(value, default)

        if not data in store:
            data = default

        combo.set_active(store.index(data))


    def __parse_configuration_file(self):
        """
        Parse configuration file
        """

        with open(self.configuration, 'r') as pipe:

            for line in pipe.readlines():
                line = line.rstrip()

                if len(line) > 0 and not line.find(';') == 0:
                    data = line.split()

                    value = " ".join(data[1:])
                    if match("(?!;).*,.*", value):
                        value = value.replace(',', '.')

                    self.data[data[0]] = value


    def option(self, option, default=None):
        """
        Get an option for a specific console
        """

        if not option in self.data.keys():
            option = "%s.%s" % (self.console, option)

        value = self.data.get(option, default)

        if type(value) is str and len(value) == 0:
            return default

        return value


    def init_values(self):
        """
        Initialize emulators values
        """

        self.__parse_configuration_file()

        # Sound
        self.check_sound.set_active(int(self.option("sound", 1)))

        self.model_sound_driver.clear()
        [self.model_sound_driver.append([driver]) for driver in self.drivers]
        self.index(self.combo_sound_driver, self.drivers,
            "sound.driver", "default")

        self.entry_sound_device.set_text(
            self.option("sound.device", "default"))

        self.scale_sound_volume.set_value(
            float(self.option("sound.volume", 100.0)))

        if self.console in self.mono_consoles:
            self.check_forcemono.set_active(int(self.option("forcemono", 0)))

        if self.console == "nes":
            self.check_less_accurate.set_active(int(self.option("n106bs", 0)))

            self.spin_rate_tolerance.set_value(
                float(self.option("sound_rate_error", .00004)))
            self.scale_sound_quality.set_value(float(self.option("soundq", 0)))

        # Video
        self.spin_resolution_x.set_value(float(self.option("xres", 0)))
        self.spin_resolution_y.set_value(float(self.option("yres", 0)))

        self.spin_scaling_x.set_value(float(self.option("xscale", 1)))
        self.spin_scaling_y.set_value(float(self.option("yscale", 1)))

        self.spin_scaling_fs_x.set_value(float(self.option("xscalefs", 1)))
        self.spin_scaling_fs_y.set_value(float(self.option("yscalefs", 1)))

        # Filters - Bilinear
        self.model_binilear.clear()
        [self.model_binilear.append([mode]) for mode in self.binilear]
        self.index(self.combo_binilear, self.binilear, "videoip", "0")

        # Filters - Stretch
        self.model_stretch.clear()
        [self.model_stretch.append([mode]) for mode in self.stretch]
        self.index(self.combo_stretch, self.stretch, "stretch", "aspect_mult2")

        # Filters - Special
        self.model_special.clear()
        [self.model_special.append([mode]) for mode in self.special]
        self.index(self.combo_special, self.special, "special", "none")

        # Filters - Shader
        self.model_shader.clear()
        [self.model_shader.append([mode]) for mode in self.pixshaders]
        self.index(self.combo_shader, self.pixshaders, "pixshader", "none")

        # Filters - Scanlines
        self.scale_scanlines.set_value(float(self.option("scanlines", 0)))

        # Blur
        self.check_temporal_blur.set_active(not self.option("tblur", 0))
        self.check_temporal_blur_accumulate.set_active(
            not self.option("tblur.accum", 0))

        self.spin_temporal_blur_amount.set_value(
            float(self.option("tblur.accum.amount", 50)))

        self.__set_sensitive()


    def get_values(self):
        """
        Return correct arguments from user selection
        """

        # Sound
        value = int(self.check_sound.get_active())
        if not int(self.option("sound", 1)) == value:
            self.__update_value("sound", value)

        value = self.combo_sound_driver.get_active_text()
        if not self.option("sound.driver", "default") == value:
            self.__update_value("sound.driver", value)

        value = self.entry_sound_device.get_text()
        if not self.option("sound.device", "default") == value:
            self.__update_value("sound.device", value)

        value = int(self.adjustment_sound_volume.get_value())
        if not int(self.option("sound.volume", 100)) == value:
            self.__update_value("sound.volume", value)

        if self.console in self.mono_consoles:
            value = int(self.check_forcemono.get_active())
            if not int(self.option("forcemono", 0)) == value:
                self.__update_value("%s.forcemono" % self.console, value)

        if self.console == "nes":
            value = int(self.check_less_accurate.get_active())
            if not int(self.option("n106bs", 0)) == value:
                self.__update_value("nes.n106bs", value)

            value = self.spin_rate_tolerance.get_value()
            if not float(self.option("sound_rate_error", .00004)) == value:
                self.__update_value("nes.sound_rate_error",
                    "{:f}".format(value))

            value = self.adjustment_sound_quality.get_value()
            if not int(self.option("soundq", 0)) == value:
                self.__update_value("nes.soundq", value)

        # Video
        value = self.spin_resolution_x.get_value()
        if not float(self.option("xres", 0)) == value:
            self.__update_value("%s.xres" % self.console, value)

        value = self.spin_resolution_y.get_value()
        if not float(self.option("yres", 0)) == value:
            self.__update_value("%s.yres" % self.console, value)

        value = self.spin_scaling_x.get_value()
        if not float(self.option("xscale", 1)) == value:
            self.__update_value("%s.xscale" % self.console, value)

        value = self.spin_scaling_y.get_value()
        if not float(self.option("yscale", 1)) == value:
            self.__update_value("%s.yscale" % self.console, value)

        value = self.spin_scaling_fs_x.get_value()
        if not float(self.option("xscalefs", 1)) == value:
            self.__update_value("%s.xscalefs" % self.console, value)

        value = self.spin_scaling_fs_y.get_value()
        if not float(self.option("yscalefs", 1)) == value:
            self.__update_value("%s.yscalefs" % self.console, value)

        # Filters - Bilinear
        value = self.combo_binilear.get_active_text()
        if not self.option("videoip", "0") == value:
            self.__update_value("%s.videoip" % self.console, value)

        # Filters - Stretch
        value = self.combo_stretch.get_active_text()
        if not self.option("stretch", "aspect_mult2") == value:
            self.__update_value("%s.stretch" % self.console, value)

        # Filters - Special
        value = self.combo_special.get_active_text()
        if not self.option("special", "none") == value:
            self.__update_value("%s.special" % self.console, value)

        # Filters - Shader
        value = self.combo_shader.get_active_text()
        if not self.option("pixshader", "none") == value:
            self.__update_value("%s.pixshader" % self.console, value)

        # Filters - Scanlines
        value = self.adjustment_scanlines.get_value()
        if not int(self.option("scanlines", 0)) == value:
            self.__update_value("%s.scanlines" % self.console, value)

        # Blur
        value = int(self.check_temporal_blur.get_active())
        if not int(self.option("tblur", 0)) == value:
            self.__update_value("%s.tblur" % self.console, value)

        value = int(self.check_temporal_blur_accumulate.get_active())
        if not int(self.option("tblur.accum", 0)) == value:
            self.__update_value("%s.tblur.accum" % self.console, value)

        value = self.spin_temporal_blur_amount.get_value()
        if not float(self.option("tblur.accum.amount", 50)) == value:
            self.__update_value("%s.tblur.accum.amount" % self.console, value)


    def __update_value(self, option, value):
        """
        Update value in main configuration file with sed
        """

        command = "sed 's/%(opt)s \(.*\)/%(opt)s %(value)s/g' -i %(path)s" % \
            dict(opt=option, value=value, path=self.configuration)

        Popen(shlex_split(command)).wait()
