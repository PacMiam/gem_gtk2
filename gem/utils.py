# -*- coding: utf-8 -*-

# ------------------------------------------------------------------
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ------------------------------------------------------------------

# ------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------

# System
from os.path import join as path_join
from os.path import exists
from os.path import expanduser

# Interface
from gtk.gdk import Pixbuf
from gtk.gdk import COLORSPACE_RGB
from gtk.gdk import pixbuf_new_from_file_at_size

# Package
from pkg_resources import require
from pkg_resources import _manager

# XDG
from xdg.BaseDirectory import xdg_data_home
from xdg.BaseDirectory import xdg_config_home

# ------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------

class Gem:
    Version     = "0.4"
    CodeName    = "Blue Bomber"
    OldColumns  = dict(
        played="play",
        last_played="last_play",
        last_played_time="last_play_time" )

class Icons:
    Ext         = "png"
    Misc        = "applications-other"
    Snap        = "emblem-photos"
    Save        = "emblem-downloads"
    File        = "emblem-documents"
    Except      = "emblem-important"
    Favorite    = "emblem-favorite"
    Game        = "input-gaming"
    System      = "preferences-system"
    Emulator    = "preferences-desktop"
    Keyboard    = "preferences-desktop-keyboard"
    Users       = "system-users"
    Multiplayer = "system-users"
    Output      = "utilities-terminal"

class Conf:
    User        = path_join(xdg_config_home, "gem")
    Data        = path_join(xdg_data_home, "gem")
    Log         = path_join("config", "log.conf")
    Default     = path_join("config", "gem.conf")
    Consoles    = path_join("config", "consoles.conf")
    Emulators   = path_join("config", "emulators.conf")
    Databases   = path_join("config", "databases.conf")

class Columns:
    Favorite        = 0
    Icon            = 1
    Name            = 2
    Played          = 3
    LastPlay        = 4
    LastTimePlay    = 5
    TimePlay        = 6
    Installed       = 7
    Except          = 8
    Snapshots       = 9
    Multiplayer     = 10
    Save            = 11
    Filename        = 12

# ------------------------------------------------------------------
#   Methods
# ------------------------------------------------------------------

def get_data(path):
    """
    Provides easy access to data in a python egg

    Thanks Deluge :)
    """

    try:
        data = require("gem")[0].get_resource_filename(
            _manager, path_join(*(["gem", path])))

        if exists(data):
            return data

        return path

    except KeyError:
        return None


def icon_from_data(icon, default=None, width=24, height=24):
    """
    Load an icon or return an empty icon if not found
    """

    if icon is not None:
        path = path_join("icons", "%s.%s" % (icon, Icons.Ext))

        if get_data(path) is not None:
            icon = expanduser(get_data(path))

        if icon is not None and exists(icon):
            try:
                return pixbuf_new_from_file_at_size(icon, width, height)
            except GError:
                pass

    if default is None:
        default = Pixbuf(COLORSPACE_RGB, True, 8, width, height)
        default.fill(0x00000000)

    return default


def set_size_request(widget, allocation):
    """
    Set a new size for a specific widget
    """

    widget.set_size_request(allocation.width, -1)


def on_entry_clear(widget, pos, event):
    """
    Reset entry filter when icon was clicked
    """

    if len(widget.get_text()) > 0:
        widget.set_text(str())
