# -*- coding: utf-8 -*-

# ------------------------------------------------------------------
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ------------------------------------------------------------------

# ------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------

# System
from os import walk
from os import remove
from os import rename
from os import listdir

from os.path import join as path_join
from os.path import exists
from os.path import dirname
from os.path import getctime
from os.path import splitext
from os.path import basename
from os.path import expanduser

from gobject import idle_add
from gobject import timeout_add
from gobject import threads_init
from gobject import source_remove

from platform import system

from datetime import datetime
from datetime import time as dtime

from copy import deepcopy

from subprocess import PIPE
from subprocess import Popen

from shlex import split as shlex_split

# File
from glob import glob

# Desktop
from xdg.Menu import parse
from xdg.Menu import Menu
from xdg.Menu import MenuEntry
from xdg.Menu import Separator
from xdg.DesktopEntry import DesktopEntry

# Translation
from gettext import lgettext as _
from gettext import textdomain
from gettext import bindtextdomain

# Interface
import gtk

from gtk.gdk import Pixbuf
from gtk.gdk import threads_enter
from gtk.gdk import threads_leave
from gtk.gdk import COLORSPACE_RGB

from pango import FontDescription

from utils import *
from windows import *
from database import Database
from emulators import *
from preferences import Preferences
from configuration import Configuration

# ------------------------------------------------------------------
#   Translation
# ------------------------------------------------------------------

bindtextdomain("gem", get_data("i18n"))
textdomain("gem")

# ------------------------------------------------------------------
#   Functions
# ------------------------------------------------------------------

def launch_gem(logger, reconstruct_db=False):
    """
    Launch GEM and manage his database
    """

    # ------------------------------------
    #   Database
    # ------------------------------------

    # Rename old databases
    if exists(expanduser(path_join(Conf.User, "games.db"))):
        rename(expanduser(path_join(Conf.User, "games.db")),
            expanduser(path_join(Conf.User, "gem.db")))

    # Move databases from ~/.config/gem to ~/.local/share/gem
    if exists(expanduser(path_join(Conf.User, "gem.db"))):
        rename(expanduser(path_join(Conf.User, "gem.db")),
            expanduser(path_join(Conf.Data, "gem.db")))

    # Connect database
    database = Database(expanduser(path_join(Conf.Data, "gem.db")),
        get_data(Conf.Databases), logger)

    version = database.select("gem", "version")
    if not version == Gem.Version:
        if version is None:
            version = Gem.Version

        database.modify("gem",
            { "version": Gem.Version, "codename": Gem.CodeName },
            { "version": version })

    # Migrate old databases
    if not database.check_integrity() or reconstruct_db:
        logger.warning(_("Current database need to be updated"))

        logger.info(_("Start database migration"))
        if database.select("games", '*') is not None:
            splash = SplashWindow(
                _("Graphical Emulators Manager"),
                _("Migrating entries from old database"),
                len(database.select("games", '*')))

            database.migrate("games", Gem.OldColumns, splash.update)

            splash.destroy()

        else:
            database.migrate("games", Gem.OldColumns)

        logger.info(_("Migration complete"))

    # ------------------------------------
    #   Exceptions
    # ------------------------------------

    # Remove old exceptions system
    if exists(expanduser(path_join(Conf.User, "exceptions.conf"))):
        exceptions = Configuration(
            expanduser(path_join(Conf.User, "exceptions.conf")))

        for section in exceptions.sections():
            database.modify("games",
                { "arguments": exceptions.get(section, "args") },
                { "filename": section })

        remove(expanduser(path_join(Conf.User, "exceptions.conf")))

    # ------------------------------------
    #   Launch interface
    # ------------------------------------

    interface = MainWindow(logger, database)

    gtk.main()

# ------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------

class MainWindow(gtk.Window):

    def __init__(self, logger, database):
        """
        Constructor
        """

        gtk.Window.__init__(self)

        # ------------------------------------
        #   Variables
        # ------------------------------------

        self.application_title = _("Graphical Emulators Manager")

        self.list_thread = int()

        self.configurators = dict()
        self.available_configurators = dict(mednafen=Mednafen)

        self.icons = dict()
        self.selection = dict()
        self.shortcuts_data = dict()

        # ------------------------------------
        #   Logger
        # ------------------------------------

        self.logger = logger

        # ------------------------------------
        #   Databases
        # ------------------------------------

        self.database = database

        # ------------------------------------
        #   Version
        # ------------------------------------

        self.application_version = Gem.Version
        self.application_version_name = Gem.CodeName

        # ------------------------------------
        #   Initialization
        # ------------------------------------

        # Init widgets
        self.__init_widgets()

        # Init signals
        self.__init_signals()

        # Start interface
        self.__start_interface()


    def __init_widgets(self):
        """
        Initialize interface widgets
        """

        # ------------------------------------
        #   Interface
        # ------------------------------------

        self.set_title("%s - %s (%s)" % (self.application_title,
            self.application_version, self.application_version_name))
        self.set_wmclass("gem", "gem")
        self.set_icon_name("joystick")

        self.set_resizable(True)
        self.set_position(gtk.WIN_POS_CENTER)
        self.set_geometry_hints(None, 1024, 768)
        self.set_default_size(1024, 768)

        self.set_mnemonics_visible(True)

        self.add_events(gtk.gdk.KEY_PRESS_MASK)

        # ------------------------------------
        #   Clipboard
        # ------------------------------------

        self.clipboard = gtk.clipboard_get(selection="CLIPBOARD")

        # ------------------------------------
        #   Icons
        # ------------------------------------

        # Get user icon theme
        self.icons_theme = gtk.icon_theme_get_default()

        self.icons_theme.append_search_path(get_data("icons/interface"))

        self.icons_data = {
            "save": Icons.Save,
            "snap": Icons.Snap,
            "except": Icons.Except,
            "favorite": Icons.Favorite,
            "multiplayer": Icons.Multiplayer }

        for icon in self.icons_data.keys():
            self.icons[icon] = self.icon_load(self.icons_data[icon], 24)

        # HACK: Create an empty image to avoid g_object_set_qdata warning
        self.empty = Pixbuf(COLORSPACE_RGB, True, 8, 24, 24)
        self.empty.fill(0x00000000)

        # ------------------------------------
        #   Shortcuts
        # ------------------------------------

        self.shortcuts_group = gtk.AccelGroup()

        # Properties
        self.add_accel_group(self.shortcuts_group)

        # ------------------------------------
        #   Grids
        # ------------------------------------

        box = gtk.VBox()

        box_logs = gtk.HBox()
        box_console = gtk.HBox()
        box_filters = gtk.HBox()
        box_informations = gtk.Table()

        self.box_tab_games = gtk.HBox()
        self.box_tab_log = gtk.HBox()

        # Properties
        box_logs.set_spacing(4)
        box_console.set_spacing(4)
        box_filters.set_spacing(4)
        box_informations.set_border_width(4)

        self.box_tab_games.set_spacing(8)
        self.box_tab_log.set_spacing(8)

        # ------------------------------------
        #   Menu
        # ------------------------------------

        self.menu = gtk.MenuBar()

        # Files
        menu_system = gtk.Menu()
        item_system = gtk.MenuItem()

        self.item_preferences = gtk.ImageMenuItem()
        self.item_quit = gtk.ImageMenuItem()

        # Game
        menu_game = gtk.Menu()
        item_game = gtk.MenuItem()

        self.item_start = gtk.ImageMenuItem()
        self.item_rename = gtk.ImageMenuItem()
        self.item_favorite = gtk.ImageMenuItem()
        self.item_multiplayer = gtk.ImageMenuItem()
        self.item_snapshots = gtk.ImageMenuItem()
        self.item_parameters = gtk.ImageMenuItem()
        self.item_copy = gtk.ImageMenuItem()
        self.item_open = gtk.ImageMenuItem()
        self.item_remove = gtk.ImageMenuItem()
        self.item_database = gtk.ImageMenuItem()

        # Help
        menu_help = gtk.Menu()
        item_help = gtk.MenuItem()

        self.item_about = gtk.ImageMenuItem()

        # Properties
        item_system.set_use_underline(True)
        item_system.set_label(_("_System"))

        self.item_preferences.set_use_underline(True)
        self.item_preferences.set_label(_("_Preferences"))
        self.item_preferences.set_image(
            gtk.image_new_from_stock(gtk.STOCK_PREFERENCES, gtk.ICON_SIZE_MENU))

        self.item_quit.set_use_underline(True)
        self.item_quit.set_label(_("_Quit"))
        self.item_quit.set_image(
            gtk.image_new_from_stock(gtk.STOCK_QUIT, gtk.ICON_SIZE_MENU))

        item_game.set_use_underline(True)
        item_game.set_label(_("_Game"))

        self.item_start.set_use_underline(True)
        self.item_start.set_label(_("_Run"))
        self.item_start.set_image(
            gtk.image_new_from_stock(gtk.STOCK_MEDIA_PLAY, gtk.ICON_SIZE_MENU))

        self.item_rename.set_use_underline(True)
        self.item_rename.set_label(_("_Rename game"))
        self.item_rename.set_image(
            gtk.image_new_from_stock(gtk.STOCK_EDIT, gtk.ICON_SIZE_MENU))

        self.item_favorite.set_use_underline(True)
        self.item_favorite.set_label(_("_Mark as favorite"))
        self.item_favorite.set_image(
            gtk.image_new_from_icon_name(Icons.Favorite, gtk.ICON_SIZE_MENU))

        self.item_multiplayer.set_use_underline(True)
        self.item_multiplayer.set_label(_("_Mark as multiplayer"))
        self.item_multiplayer.set_image(
            gtk.image_new_from_icon_name(Icons.Multiplayer, gtk.ICON_SIZE_MENU))

        self.item_snapshots.set_use_underline(True)
        self.item_snapshots.set_label(_("_Show snapshots"))
        self.item_snapshots.set_image(
            gtk.image_new_from_icon_name(Icons.Snap, gtk.ICON_SIZE_MENU))

        self.item_parameters.set_use_underline(True)
        self.item_parameters.set_label(_("_Set parameters"))
        self.item_parameters.set_image(
            gtk.image_new_from_icon_name(Icons.Except, gtk.ICON_SIZE_MENU))

        self.item_copy.set_use_underline(True)
        self.item_copy.set_label(_("_Copy game path"))
        self.item_copy.set_image(
            gtk.image_new_from_stock(gtk.STOCK_COPY, gtk.ICON_SIZE_MENU))

        self.item_open.set_use_underline(True)
        self.item_open.set_label(_("_Open directory path"))
        self.item_open.set_image(
            gtk.image_new_from_stock(gtk.STOCK_OPEN, gtk.ICON_SIZE_MENU))

        self.item_remove.set_use_underline(True)
        self.item_remove.set_label(_("_Remove from disk"))
        self.item_remove.set_image(
            gtk.image_new_from_stock(gtk.STOCK_DELETE, gtk.ICON_SIZE_MENU))

        self.item_database.set_use_underline(True)
        self.item_database.set_label(_("_Remove from database"))
        self.item_database.set_image(
            gtk.image_new_from_stock(gtk.STOCK_CLEAR, gtk.ICON_SIZE_MENU))

        item_help.set_use_underline(True)
        item_help.set_label(_("_Help"))

        self.item_about.set_use_underline(True)
        self.item_about.set_label(_("_About"))
        self.item_about.set_image(
            gtk.image_new_from_stock(gtk.STOCK_ABOUT, gtk.ICON_SIZE_MENU))

        # ------------------------------------
        #   Toolbar
        # ------------------------------------

        toolbar = gtk.Toolbar()

        self.tool_start = gtk.ToolButton()
        self.tool_fullscreen = gtk.ToggleToolButton()
        self.tool_snapshots = gtk.ToolButton()
        self.tool_parameters = gtk.ToolButton()
        self.tool_preferences = gtk.ToolButton()
        self.tool_about = gtk.ToolButton()
        self.tool_quit = gtk.ToolButton()

        tool_separator = gtk.SeparatorToolItem()

        tool_combo = gtk.ToolItem()
        self.tool_emulator = gtk.ToolButton()

        # Properties
        toolbar.set_style(gtk.TOOLBAR_ICONS)
        toolbar.set_orientation(gtk.ORIENTATION_HORIZONTAL)

        self.tool_start.set_label(_("Play"))
        self.tool_start.set_stock_id(gtk.STOCK_MEDIA_PLAY)
        self.tool_start.set_tooltip_text(_("Run selected game"))

        self.tool_fullscreen.set_stock_id(gtk.STOCK_FULLSCREEN)
        self.tool_fullscreen.set_tooltip_text(_("Use fullscreen display mode"))

        self.tool_snapshots.set_label(_("Snapshots"))
        self.tool_snapshots.set_icon_name(Icons.Snap)
        self.tool_snapshots.set_tooltip_text(_("Show selected game snapshots"))

        self.tool_parameters.set_label(_("Parameters"))
        self.tool_parameters.set_icon_name(Icons.Except)
        self.tool_parameters.set_tooltip_text(
            _("Set parameters for selected game"))

        self.tool_preferences.set_stock_id(gtk.STOCK_PREFERENCES)
        self.tool_preferences.set_tooltip_text(_("Preferences"))

        self.tool_about.set_stock_id(gtk.STOCK_ABOUT)
        self.tool_about.set_tooltip_text(_("About"))

        self.tool_quit.set_stock_id(gtk.STOCK_QUIT)
        self.tool_quit.set_tooltip_text(_("Quit"))

        tool_separator.set_expand(True)
        tool_separator.set_draw(False)

        tool_combo.set_border_width(2)

        self.tool_emulator.set_stock_id(gtk.STOCK_PROPERTIES)
        self.tool_emulator.set_tooltip_text(_("Configure emulator"))

        # ------------------------------------
        #   Toolbar - Consoles
        # ------------------------------------

        label_console = gtk.Label()

        self.model_console = gtk.ListStore(Pixbuf, str)
        self.combo_console = gtk.ComboBox()

        cell_console_image = gtk.CellRendererPixbuf()
        cell_console_name = gtk.CellRendererText()

        # Properties
        label_console.set_use_markup(True)
        label_console.set_alignment(1, .5)
        label_console.set_markup("<b>%s</b>:" % _("Console"))

        self.combo_console.set_model(self.model_console)

        self.combo_console.pack_start(cell_console_image, False)
        self.combo_console.add_attribute(cell_console_image, "pixbuf", 0)
        self.combo_console.pack_start(cell_console_name, True)
        self.combo_console.set_attributes(cell_console_name, text=1)

        self.model_console.set_sort_column_id(1, gtk.SORT_ASCENDING)

        cell_console_image.set_padding(4, 0)

        # ------------------------------------
        #   Toolbar - Filters
        # ------------------------------------

        label_filter = gtk.Label()

        self.tool_filter_favorite = gtk.ToggleToolButton()
        self.tool_filter_multiplayer = gtk.ToggleToolButton()

        tool_entry = gtk.ToolItem()
        self.entry_filter = gtk.Entry()

        # Properties
        label_filter.set_use_markup(True)
        label_filter.set_alignment(1, .5)
        label_filter.set_markup("<b>%s</b>:" % _("Filter"))

        self.tool_filter_favorite.set_label(_("Favorite"))
        self.tool_filter_favorite.set_icon_name(Icons.Favorite)
        self.tool_filter_favorite.set_tooltip_text(
            _("Show favorite games"))

        self.tool_filter_multiplayer.set_label(_("Multiplayer"))
        self.tool_filter_multiplayer.set_icon_name(Icons.Multiplayer)
        self.tool_filter_multiplayer.set_tooltip_text(
            _("Show multiplayer games"))

        tool_entry.set_border_width(2)

        self.entry_filter.set_size_request(300, -1)
        self.entry_filter.set_icon_from_stock(0, gtk.STOCK_FIND)
        self.entry_filter.set_icon_activatable(0, False)
        self.entry_filter.set_icon_from_stock(1, gtk.STOCK_CLEAR)

        # ------------------------------------
        #   Informations bar
        # ------------------------------------

        self.informations = gtk.InfoBar()
        self.label_informations = gtk.Label()

        # Properties
        self.informations.get_content_area().add(self.label_informations)

        self.label_informations.set_use_markup(True)

        # ------------------------------------
        #   Notebook
        # ------------------------------------

        self.notebook = gtk.Notebook()

        label_tab_games = gtk.Label()
        image_tab_games = gtk.Image()

        label_tab_log = gtk.Label()
        image_tab_log = gtk.Image()

        # Properties
        self.notebook.set_border_width(4)
        self.notebook.set_property("homogeneous", True)

        label_tab_games.set_text(_("Games"))
        image_tab_games.set_from_icon_name(Icons.Game, gtk.ICON_SIZE_MENU)

        label_tab_log.set_text(_("Output"))
        image_tab_log.set_from_icon_name(Icons.Output, gtk.ICON_SIZE_MENU)

        # ------------------------------------
        #   Games - List
        # ------------------------------------

        scroll_games = gtk.ScrolledWindow()

        self.model_games = gtk.ListStore(
            bool,   # Favorite status
            Pixbuf, # Favorite icon
            str,    # Name
            str,    # Number of play
            str,    # Last play date
            str,    # Last play time
            str,    # Play time
            str,    # Append date
            Pixbuf, # Special configuration icon
            Pixbuf, # Save state icon
            Pixbuf, # Snapshots icon
            Pixbuf, # Multiplayer icon
            str)    # Filename
        self.treeview_games = gtk.TreeView()

        self.filter_games = self.model_games.filter_new()

        column_game_favorite = gtk.TreeViewColumn()
        column_game_name = gtk.TreeViewColumn()
        self.column_game_play = gtk.TreeViewColumn()
        self.column_game_last_play = gtk.TreeViewColumn()
        self.column_game_play_time = gtk.TreeViewColumn()
        self.column_game_installed_date = gtk.TreeViewColumn()
        self.column_game_flags = gtk.TreeViewColumn()

        cell_game_favorite = gtk.CellRendererPixbuf()
        cell_game_name = gtk.CellRendererText()
        cell_game_play = gtk.CellRendererText()
        cell_game_last_play = gtk.CellRendererText()
        cell_game_last_play_time = gtk.CellRendererText()
        cell_game_play_time = gtk.CellRendererText()
        cell_game_installed_date = gtk.CellRendererText()
        cell_game_except = gtk.CellRendererPixbuf()
        cell_game_save = gtk.CellRendererPixbuf()
        cell_game_snap = gtk.CellRendererPixbuf()
        cell_game_multiplayer = gtk.CellRendererPixbuf()

        # Properties
        scroll_games.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

        self.model_games.set_sort_column_id(Columns.Name, gtk.SORT_ASCENDING)

        self.treeview_games.set_rules_hint(True)
        self.treeview_games.set_headers_visible(True)
        self.treeview_games.set_enable_tree_lines(True)
        self.treeview_games.set_model(self.filter_games)

        column_game_favorite.set_resizable(False)
        column_game_favorite.pack_start(cell_game_favorite, False)
        column_game_favorite.add_attribute(
            cell_game_favorite, "pixbuf", Columns.Icon)

        column_game_name.set_expand(True)
        column_game_name.set_title(_("Name"))
        column_game_name.set_resizable(True)
        column_game_name.set_sizing(gtk.TREE_VIEW_COLUMN_FIXED)
        column_game_name.pack_start(cell_game_name, True)
        column_game_name.set_attributes(cell_game_name, text=Columns.Name)

        self.column_game_play.set_alignment(.5)
        self.column_game_play.set_title(_("Played"))
        self.column_game_play.pack_start(cell_game_play, True)
        self.column_game_play.set_attributes(
            cell_game_play, text=Columns.Played)

        self.column_game_last_play.set_spacing(5)
        self.column_game_last_play.set_alignment(.5)
        self.column_game_last_play.set_title(_("Last played"))
        self.column_game_last_play.pack_start(cell_game_last_play, True)
        self.column_game_last_play.pack_start(cell_game_last_play_time, True)
        self.column_game_last_play.add_attribute(
            cell_game_last_play, "text", Columns.LastPlay)
        self.column_game_last_play.add_attribute(
            cell_game_last_play_time, "text", Columns.LastTimePlay)

        self.column_game_play_time.set_alignment(.5)
        self.column_game_play_time.set_title(_("Time played"))
        self.column_game_play_time.pack_start(cell_game_play_time, True)
        self.column_game_play_time.set_attributes(
            cell_game_play_time, text=Columns.TimePlay)

        self.column_game_installed_date.set_alignment(.5)
        self.column_game_installed_date.set_title(_("Installed"))
        self.column_game_installed_date.pack_start(
            cell_game_installed_date, True)
        self.column_game_installed_date.set_attributes(
            cell_game_installed_date, text=Columns.Installed)

        self.column_game_flags.set_spacing(4)
        self.column_game_flags.set_alignment(.5)
        self.column_game_flags.set_title(_("Flags"))
        self.column_game_flags.pack_start(cell_game_except, False)
        self.column_game_flags.pack_start(cell_game_snap, False)
        self.column_game_flags.pack_start(cell_game_multiplayer, False)
        self.column_game_flags.pack_start(cell_game_save, False)
        self.column_game_flags.add_attribute(
            cell_game_except, "pixbuf", Columns.Except)
        self.column_game_flags.add_attribute(
            cell_game_snap, "pixbuf", Columns.Snapshots)
        self.column_game_flags.add_attribute(
            cell_game_multiplayer, "pixbuf", Columns.Multiplayer)
        self.column_game_flags.add_attribute(
            cell_game_save, "pixbuf", Columns.Save)

        cell_game_play.set_alignment(.5, .5)
        cell_game_last_play.set_alignment(0, .5)
        cell_game_last_play_time.set_alignment(1, .5)
        cell_game_play_time.set_alignment(.5, .5)
        cell_game_installed_date.set_alignment(.5, .5)

        cell_game_favorite.set_padding(4, 0)
        cell_game_name.set_padding(10, 0)
        cell_game_play.set_padding(10, 0)
        cell_game_last_play.set_padding(10, 0)
        cell_game_last_play_time.set_padding(10, 0)
        cell_game_play_time.set_padding(10, 0)
        cell_game_installed_date.set_padding(10, 0)
        cell_game_except.set_padding(2, 0)
        cell_game_snap.set_padding(2, 0)
        cell_game_multiplayer.set_padding(2, 0)
        cell_game_save.set_padding(2, 0)

        self.treeview_games.append_column(column_game_favorite)
        self.treeview_games.append_column(column_game_name)
        self.treeview_games.append_column(self.column_game_play)
        self.treeview_games.append_column(self.column_game_last_play)
        self.treeview_games.append_column(self.column_game_play_time)
        self.treeview_games.append_column(self.column_game_installed_date)
        self.treeview_games.append_column(self.column_game_flags)

        # ------------------------------------
        #   Games - Menu
        # ------------------------------------

        self.menu_game = gtk.Menu()

        self.item_game_start = gtk.ImageMenuItem()
        self.item_game_renamed = gtk.ImageMenuItem()
        self.item_game_favorite = gtk.ImageMenuItem()
        self.item_game_multiplayer = gtk.ImageMenuItem()
        self.item_game_snapshots = gtk.ImageMenuItem()
        self.item_game_parameters = gtk.ImageMenuItem()
        self.item_game_copy = gtk.ImageMenuItem()
        self.item_game_open = gtk.ImageMenuItem()
        self.item_game_database = gtk.ImageMenuItem()
        self.item_game_remove = gtk.ImageMenuItem()

        # Properties
        self.item_game_start.set_use_underline(True)
        self.item_game_start.set_label(_("_Play"))
        self.item_game_start.set_image(
            gtk.image_new_from_stock(gtk.STOCK_MEDIA_PLAY, gtk.ICON_SIZE_MENU))

        self.item_game_renamed.set_use_underline(True)
        self.item_game_renamed.set_label(_("_Rename"))
        self.item_game_renamed.set_image(
            gtk.image_new_from_stock(gtk.STOCK_EDIT, gtk.ICON_SIZE_MENU))

        self.item_game_favorite.set_use_underline(True)
        self.item_game_favorite.set_label(_("_Mark as favorite"))
        self.item_game_favorite.set_image(
            gtk.image_new_from_icon_name(Icons.Favorite, gtk.ICON_SIZE_MENU))

        self.item_game_multiplayer.set_use_underline(True)
        self.item_game_multiplayer.set_label(_("_Mark as multiplayer"))
        self.item_game_multiplayer.set_image(
            gtk.image_new_from_icon_name(Icons.Multiplayer, gtk.ICON_SIZE_MENU))

        self.item_game_snapshots.set_use_underline(True)
        self.item_game_snapshots.set_label(_("_Show snapshots"))
        self.item_game_snapshots.set_image(
            gtk.image_new_from_icon_name(Icons.Snap, gtk.ICON_SIZE_MENU))

        self.item_game_parameters.set_use_underline(True)
        self.item_game_parameters.set_label(_("_Set parameters"))
        self.item_game_parameters.set_image(
            gtk.image_new_from_icon_name(Icons.Except, gtk.ICON_SIZE_MENU))

        self.item_game_copy.set_use_underline(True)
        self.item_game_copy.set_label(_("_Copy game path"))
        self.item_game_copy.set_image(
            gtk.image_new_from_stock(gtk.STOCK_COPY, gtk.ICON_SIZE_MENU))

        self.item_game_open.set_use_underline(True)
        self.item_game_open.set_label(_("_Open directory path"))
        self.item_game_open.set_image(
            gtk.image_new_from_stock(gtk.STOCK_OPEN, gtk.ICON_SIZE_MENU))

        self.item_game_remove.set_use_underline(True)
        self.item_game_remove.set_label(_("_Remove game from disk"))
        self.item_game_remove.set_image(
            gtk.image_new_from_stock(gtk.STOCK_DELETE, gtk.ICON_SIZE_MENU))

        self.item_game_database.set_use_underline(True)
        self.item_game_database.set_label(_("_Remove entry from database"))
        self.item_game_database.set_image(
            gtk.image_new_from_stock(gtk.STOCK_CLEAR, gtk.ICON_SIZE_MENU))

        # ------------------------------------
        #   Logs
        # ------------------------------------

        self.log = gtk.TextView()
        scroll_log = gtk.ScrolledWindow()

        # Properties
        self.log.set_editable(False)
        self.log.set_overwrite(False)
        self.log.set_size_request(-1, 200)
        self.log.modify_font(FontDescription("Monospace"))

        scroll_log.set_border_width(1)
        scroll_log.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

        # ------------------------------------
        #   Statusbar
        # ------------------------------------

        self.statusbar = gtk.Statusbar()

        box_statusbar = self.statusbar.get_message_area()
        self.label_statusbar = box_statusbar.get_children()[0]

        # Properties
        self.statusbar.set_has_resize_grip(False)

        box_statusbar.set_border_width(2)
        self.label_statusbar.set_use_markup(True)

        # ------------------------------------
        #   Add widgets into interface
        # ------------------------------------

        self.add(box)

        # Interface
        box.pack_start(self.menu, False)
        box.pack_start(toolbar, False)
        box.pack_start(self.informations, False)
        box.pack_start(self.notebook, True)
        box.pack_start(self.statusbar, False)

        # box_console.pack_start(label_console, False)
        box_console.pack_start(self.combo_console, False)

        # box_filters.pack_start(label_filter, False)
        box_filters.pack_start(self.entry_filter, False)

        # Menu
        self.menu.add(item_system)
        self.menu.add(item_game)
        self.menu.add(item_help)

        item_system.set_submenu(menu_system)
        item_game.set_submenu(menu_game)
        item_help.set_submenu(menu_help)

        menu_system.add(self.item_preferences)
        menu_system.add(gtk.SeparatorMenuItem())
        menu_system.add(self.item_quit)

        menu_game.add(self.item_start)
        menu_game.add(gtk.SeparatorMenuItem())
        menu_game.add(self.item_rename)
        menu_game.add(self.item_favorite)
        menu_game.add(self.item_multiplayer)
        menu_game.add(self.item_snapshots)
        menu_game.add(self.item_parameters)
        menu_game.add(gtk.SeparatorMenuItem())
        menu_game.add(self.item_copy)
        menu_game.add(self.item_open)
        menu_game.add(gtk.SeparatorMenuItem())
        menu_game.add(self.item_database)
        menu_game.add(self.item_remove)

        menu_help.add(self.item_about)

        # Toolbar
        toolbar.insert(self.tool_start, -1)
        toolbar.insert(gtk.SeparatorToolItem(), -1)
        toolbar.insert(self.tool_fullscreen, -1)
        toolbar.insert(self.tool_parameters, -1)
        toolbar.insert(self.tool_snapshots, -1)
        toolbar.insert(gtk.SeparatorToolItem(), -1)
        toolbar.insert(self.tool_preferences, -1)
        toolbar.insert(gtk.SeparatorToolItem(), -1)
        # toolbar.insert(self.tool_about, -1)
        toolbar.insert(self.tool_quit, -1)
        toolbar.insert(tool_separator, -1)
        toolbar.insert(tool_entry, -1)
        toolbar.insert(self.tool_filter_favorite, -1)
        toolbar.insert(self.tool_filter_multiplayer, -1)
        toolbar.insert(gtk.SeparatorToolItem(), -1)
        toolbar.insert(tool_combo, -1)
        toolbar.insert(self.tool_emulator, -1)

        tool_combo.add(box_console)
        tool_entry.add(box_filters)

        # Games
        self.menu_game.add(self.item_game_start)
        self.menu_game.add(gtk.SeparatorMenuItem())
        self.menu_game.add(self.item_game_renamed)
        self.menu_game.add(self.item_game_favorite)
        self.menu_game.add(self.item_game_multiplayer)
        self.menu_game.add(self.item_game_snapshots)
        self.menu_game.add(self.item_game_parameters)
        self.menu_game.add(gtk.SeparatorMenuItem())
        self.menu_game.add(self.item_game_copy)
        self.menu_game.add(self.item_game_open)
        self.menu_game.add(gtk.SeparatorMenuItem())
        self.menu_game.add(self.item_game_database)
        self.menu_game.add(self.item_game_remove)

        # Logs
        box_logs.pack_start(scroll_log, True)

        # Notebook
        scroll_games.add(self.treeview_games)
        scroll_log.add(self.log)

        self.box_tab_games.pack_start(image_tab_games, False)
        self.box_tab_games.pack_start(label_tab_games, False)

        self.box_tab_log.pack_start(image_tab_log, False)
        self.box_tab_log.pack_start(label_tab_log, False)

        self.notebook.append_page(scroll_games, self.box_tab_games)
        self.notebook.append_page(box_logs, self.box_tab_log)


    def __init_signals(self):
        """
        Initialize widgets signals
        """

        # Interface
        self.connect("destroy", self.__stop_interface)
        self.connect("key-press-event", self.__manage_keys)

        # Menu
        self.item_start.connect("activate", self.__on_game_launch)
        self.item_rename.connect("activate", self.__on_game_renamed)
        self.item_favorite.connect("activate",
            self.__on_game_marked_as_favorite)
        self.item_multiplayer.connect("activate",
            self.__on_game_marked_as_multiplayer)
        self.item_snapshots.connect("activate", self.__on_game_snaps)
        self.item_parameters.connect("activate", self.__on_game_parameters)
        self.item_copy.connect("activate", self.__on_game_copy)
        self.item_open.connect("activate", self.__on_game_open)
        self.item_remove.connect("activate", self.__on_game_removed)
        self.item_database.connect("activate", self.__on_game_clean)
        self.item_preferences.connect("activate", Preferences, self)
        self.item_quit.connect("activate", self.__stop_interface)
        self.item_about.connect("activate", AboutDialog, self)

        # Toolbar
        self.tool_start.connect("clicked", self.__on_game_launch)
        self.tool_fullscreen.connect("toggled", self.__on_activate_fullscreen)
        self.tool_snapshots.connect("clicked", self.__on_game_snaps)
        self.tool_parameters.connect("clicked", self.__on_game_parameters)
        self.tool_preferences.connect("clicked", Preferences, self)
        self.tool_about.connect("clicked", AboutDialog, self)
        self.tool_quit.connect("clicked", self.__stop_interface)
        self.tool_emulator.connect("clicked", Window.Editor, self)
        self.tool_filter_favorite.connect("clicked", self.filters_update)
        self.tool_filter_multiplayer.connect("clicked", self.filters_update)

        self.entry_filter.connect("icon-press", on_entry_clear)
        self.entry_filter.connect("changed", self.filters_update)

        # Consoles
        self.combo_console.connect("changed", self.__on_console_selected)

        # Games
        self.treeview_games.connect("button-press-event",
            self.__on_game_selected)
        self.treeview_games.connect("key-release-event",
            self.__on_game_selected)

        self.treeview_games.connect("button_press_event",
            self.__on_menu_show, self.menu_game)

        self.item_game_copy.connect("activate",
            self.__on_game_copy)
        self.item_game_open.connect("activate",
            self.__on_game_open)
        self.item_game_start.connect("activate",
            self.__on_game_launch)
        self.item_game_renamed.connect("activate",
            self.__on_game_renamed)
        self.item_game_favorite.connect("activate",
            self.__on_game_marked_as_favorite)
        self.item_game_multiplayer.connect("activate",
            self.__on_game_marked_as_multiplayer)
        self.item_game_snapshots.connect("activate",
            self.__on_game_snaps)
        self.item_game_parameters.connect("activate",
            self.__on_game_parameters)
        self.item_game_remove.connect("activate",
            self.__on_game_removed)
        self.item_game_database.connect("activate",
            self.__on_game_clean)

        self.filter_games.set_visible_func(self.filters_match)


    def __init_shortcuts(self):
        """
        Generate shortcuts signals from user configuration
        """

        shortcuts = {
            self.item_open:
                self.config.item("keys", "open", "<Control>O"),
            self.item_game_open:
                self.config.item("keys", "open", "<Control>O"),
            self.item_copy:
                self.config.item("keys", "copy", "<Control>C"),
            self.item_game_copy:
                self.config.item("keys", "copy", "<Control>C"),
            self.tool_start:
                self.config.item("keys", "start", "Return"),
            self.item_start:
                self.config.item("keys", "start", "Return"),
            self.item_game_start:
                self.config.item("keys", "start", "Return"),
            self.item_rename:
                self.config.item("keys", "rename", "F2"),
            self.item_game_renamed:
                self.config.item("keys", "rename", "F2"),
            self.item_favorite:
                self.config.item("keys", "favorite", "F3"),
            self.item_game_favorite:
                self.config.item("keys", "favorite", "F3"),
            self.item_multiplayer:
                self.config.item("keys", "multiplayer", "F4"),
            self.item_game_multiplayer:
                self.config.item("keys", "multiplayer", "F4"),
            self.tool_snapshots:
                self.config.item("keys", "snapshots", "F5"),
            self.item_snapshots:
                self.config.item("keys", "snapshots", "F5"),
            self.item_game_snapshots:
                self.config.item("keys", "snapshots", "F5"),
            self.tool_parameters:
                self.config.item("keys", "exceptions", "F12"),
            self.item_parameters:
                self.config.item("keys", "exceptions", "F12"),
            self.item_game_parameters:
                self.config.item("keys", "exceptions", "F12"),
            self.item_remove:
                self.config.item("keys", "delete", "<Control>Delete"),
            self.item_game_remove:
                self.config.item("keys", "delete", "<Control>Delete"),
            self.item_database:
                self.config.item("keys", "remove", "Delete"),
            self.item_game_database:
                self.config.item("keys", "remove", "Delete"),
            self.tool_preferences:
                self.config.item("keys", "preferences", "<Control>P"),
            self.item_preferences:
                self.config.item("keys", "preferences", "<Control>P"),
            self.tool_quit:
                self.config.item("keys", "quit", "<Control>Q"),
            self.item_quit:
                self.config.item("keys", "quit", "<Control>Q") }

        for widget in shortcuts.keys():
            key, mod = gtk.accelerator_parse(shortcuts[widget])

            if gtk.accelerator_valid(key, mod):

                if self.shortcuts_data.get(widget) is not None:
                    old_key, old_mod = self.shortcuts_data.get(widget)

                    widget.remove_accelerator(self.shortcuts_group,
                        old_key, old_mod)

                if type(widget) == gtk.Entry:
                    widget.add_accelerator("grab_focus",
                        self.shortcuts_group, key, mod, gtk.ACCEL_MASK)

                elif type(widget) == gtk.ToolButton:
                    widget.add_accelerator("clicked",
                        self.shortcuts_group, key, mod, gtk.ACCEL_VISIBLE)

                else:
                    widget.add_accelerator("activate",
                        self.shortcuts_group, key, mod, gtk.ACCEL_VISIBLE)

                self.shortcuts_data[widget] = [key, mod]


    def __manage_keys(self, widget, event):
        """
        Manage widgets for specific keymaps
        """

        if gtk.gdk.keyval_name(event.keyval) == "F11":
            self.tool_fullscreen.set_active(
                not self.tool_fullscreen.get_active())


    def __start_interface(self):
        """
        Load data and start interface
        """

        self.load_interface()

        if bool(int(self.config.item("gem", "load_console_startup", 1))):
            console = self.config.item("gem", "last_console", str())
            if len(console) > 0:
                for row in self.model_console:
                    if row[1] == console:
                        self.treeview_games.set_visible(True)
                        self.combo_console.set_active_iter(row.iter)
                        self.selection["console"] = console
                        break

        if bool(int(self.config.item("gem", "welcome", 1))):
            dialog = Dialog.Message(self, _("Welcome !"), _("GEM come with "
                "some emulators and consoles already configured. But the "
                "default paths may not work with your system.\n\nThe first "
                "thing you may do, it's open the preferences window and "
                "change paths for both consoles and emulators.\n\nEnjoy your "
                "games :D"))
            dialog.start()

            self.config.modify("gem", "welcome", 0)
            self.config.update()


    def __stop_interface(self, widget=None):
        """
        Save data and stop interface
        """

        if not self.list_thread == 0:
            source_remove(self.list_thread)

        row = self.combo_console.get_active_iter()
        if row is not None:
            self.config.modify("gem", "last_console",
                self.model_console.get_value(row, 1))
            self.config.update()

        gtk.main_quit()


    def load_interface(self):
        """
        Load main interface
        """

        # ------------------------------------
        #   Configuration
        # ------------------------------------

        self.config = Configuration(expanduser(
            path_join(Conf.User, "gem.conf")))
        self.config.add_missing_data(get_data(Conf.Default))

        self.emulators = Configuration(
            expanduser(path_join(Conf.User, "emulators.conf")))

        self.consoles = Configuration(
            expanduser(path_join(Conf.User, "consoles.conf")))

        # ------------------------------------
        #   Shortcuts
        # ------------------------------------

        self.__init_shortcuts()

        # ------------------------------------
        #   Widgets
        # ------------------------------------

        current_console = self.append_consoles()

        self.show_all()
        self.menu_game.show_all()
        self.box_tab_games.show_all()
        self.box_tab_log.show_all()

        self.sensitive_interface()

        self.informations.hide()

        # Logs
        if not bool(int(self.config.item("gem", "show_output", 1))):
            self.notebook.set_show_tabs(False)
            self.notebook.set_show_border(False)
        else:
            self.notebook.set_show_tabs(True)
            self.notebook.set_show_border(True)

        # Statusbar
        if not bool(int(self.config.item("gem", "show_statusbar", 1))):
            self.statusbar.hide()
        else:
            self.statusbar.show_all()

        # Games - Treeview
        lines = {
            "none": gtk.TREE_VIEW_GRID_LINES_NONE,
            "horizontal": gtk.TREE_VIEW_GRID_LINES_HORIZONTAL,
            "vertical": gtk.TREE_VIEW_GRID_LINES_VERTICAL,
            "both": gtk.TREE_VIEW_GRID_LINES_BOTH }

        if self.config.item("gem", "games_treeview_lines", "none") in lines:
            self.treeview_games.set_grid_lines(
                lines[self.config.item("gem", "games_treeview_lines", "none")])

        # Games - Treeview columns
        columns = {
            "play": self.column_game_play,
            "last_play": self.column_game_last_play,
            "play_time": self.column_game_play_time,
            "installed": self.column_game_installed_date,
            "flags": self.column_game_flags }

        for key, widget in columns.items():
            if not bool(int(self.config.item("columns", key, 1))):
                widget.set_visible(False)
            else:
                widget.set_visible(True)

        if current_console is None:
            self.treeview_games.set_visible(False)
            self.selection = dict(console=None, game=None)

        else:
            self.treeview_games.set_visible(True)
            self.combo_console.set_active_iter(current_console)
            self.selection["game"] = None

        self.statusbar_update()


    def sensitive_interface(self, status=False):
        """
        Set a sensitive status for specific widgets
        """

        self.item_start.set_sensitive(status)
        self.item_rename.set_sensitive(status)
        self.item_favorite.set_sensitive(status)
        self.item_multiplayer.set_sensitive(status)
        self.item_snapshots.set_sensitive(status)
        self.item_parameters.set_sensitive(status)
        self.item_copy.set_sensitive(status)
        self.item_open.set_sensitive(status)
        self.item_remove.set_sensitive(status)
        self.item_database.set_sensitive(status)

        self.item_game_renamed.set_sensitive(status)
        self.item_game_favorite.set_sensitive(status)
        self.item_game_multiplayer.set_sensitive(status)
        self.item_game_snapshots.set_sensitive(status)
        self.item_game_copy.set_sensitive(status)
        self.item_game_open.set_sensitive(status)
        self.item_game_remove.set_sensitive(status)
        self.item_game_database.set_sensitive(status)

        self.tool_start.set_sensitive(status)
        self.tool_parameters.set_sensitive(status)
        self.tool_snapshots.set_sensitive(status)


    def statusbar_update(self):
        """
        Update text from statusbar
        """

        text = list()

        if self.selection.get("console") is not None:
            text.append("<b>%s</b>: %d roms" % (
                self.selection.get("console"), len(self.model_games)))

            text.append("<b>%s</b>: %s" % (_("Emulator"),
                self.consoles.item(self.selection.get("console"), "emulator")))

        if self.selection.get("game") is not None:
            text.append("<b>%s</b>: %s" % (_("ROM"),
                basename(self.selection.get("game")).replace('&', "&amp;")))

        self.label_statusbar.set_markup(" | ".join(text))


    def __on_menu_show(self, treeview, event, menu):
        """
        Show a menu when user right-click in treeview
        """

        if event.button == 3:

            treeiter = treeview.get_path_at_pos(int(event.x), int(event.y))

            if treeiter is not None:
                path, col = treeiter[0:2]

                treeview.grab_focus()
                treeview.set_cursor(path, col, 0)

                menu.popup(None, None, None, event.button, event.time)

            return True


    def append_consoles(self):
        """
        Fill consoles treeview with user data
        """

        item = None

        self.model_console.clear()

        for console in self.consoles.sections():

            if self.consoles.has_option(console, "emulator"):
                emulator = self.consoles.get(console, "emulator")

                # Check if current emulator can be launched
                binary = self.emulators.item(emulator, "binary")
                if binary is not None and exists(binary):
                    row = self.model_console.append([icon_from_data(
                        self.consoles.item(console, "icon"), self.empty),
                        console])

                    if self.selection.get("console") is not None and \
                        self.selection.get("console") == console:
                        item = row

                    if emulator in self.available_configurators:
                        configurator = self.available_configurators[emulator]
                        self.configurators[console] = configurator(console)

                else:
                    self.logger.warning(
                        _("Cannot find %(binary)s for %(console)s" % dict(
                        binary=binary, console=console)))

        return item


    def __on_console_selected(self, widget=None):
        """
        Select a console in consoles treeview
        """

        self.treeview_games.set_visible(True)

        error = False
        self.informations.hide_all()

        treeiter = self.combo_console.get_active_iter()
        if treeiter is not None:

            console = self.model_console.get_value(treeiter, 1)
            if console is not None:

                self.selection["console"] = console

                # ------------------------------------
                #   Check emulator
                # ------------------------------------

                if not self.consoles.has_option(console, "emulator"):
                    message = _("Cannot find emulator for %s") % console
                    error = True

                emulator = self.consoles.get(console, "emulator")

                # Check emulator data
                if not self.emulators.has_section(emulator):
                    message = _("<b>%s</b> emulator not exist !") % (emulator)
                    error = True

                # Check binary
                if not exists(self.emulators.item(emulator, "binary")):
                    message = _("Cannot find <b>%s</b> !") % (
                        self.emulators.item(emulator, "binary"))
                    error = True

                # Check ROMs path
                if not exists(self.consoles.item(console, "roms")):
                    message = _("Cannot find ROMs path <b>%s</b> !") % (
                        self.consoles.item(console, "roms"))
                    error = True

                # Check emulator configurator
                if emulator in self.available_configurators.keys():
                    self.selection["emulator"] = self.available_configurators[
                        emulator]

                else:
                    self.selection["emulator"] = None

                # ------------------------------------
                #   Load game list
                # ------------------------------------

                if error:
                    self.model_games.clear()
                    self.filter_games.refilter()

                    self.treeview_games.set_visible(False)

                    self.set_error(message)

                else:
                    if not self.list_thread == 0:
                        source_remove(self.list_thread)

                    loader = self.append_games(console)
                    self.list_thread = idle_add(loader.next)

                    self.selection["game"] = None

        self.statusbar_update()


    def append_games(self, index):
        """
        Fill games treeview with console data
        """

        iteration = int()

        # Get current thread id
        current_thread_id = self.list_thread

        self.game_path = dict()

        games_path = expanduser(self.consoles.get(index, "roms"))

        # ------------------------------------
        #   Load data
        # ------------------------------------

        emulator = self.consoles.get(index, "emulator")

        if exists(games_path):
            games_list = list()

            for root, dirnames, filenames in walk(games_path):
                for path in filenames:
                    games_list.append(path_join(root, path))

            sorted(games_list)

            # ------------------------------------
            #   Refresh treeview
            # ------------------------------------

            self.model_games.clear()
            self.filter_games.refilter()

            self.treeview_games.set_visible(True)
            self.treeview_games.set_enable_search(False)
            self.treeview_games.freeze_child_notify()

            yield True

            # ------------------------------------
            #   Load games
            # ------------------------------------

            for game in games_list:

                # Another thread has been called by user, close this one
                if not current_thread_id == self.list_thread:
                    yield False

                if '.' in basename(game):
                    filename, ext = splitext(basename(game))

                    # Remove dot from extension
                    ext = ext.split('.')[-1]

                    # Get extensions list for current console
                    ext_list = self.consoles.get(index, "exts").split(';')

                    # Check lowercase extensions
                    if ext in ext_list or ext.lower() in ext_list:
                        favorite = False
                        custom_name = filename
                        icon = self.empty
                        exception = self.empty
                        multiplayer = self.empty
                        number, last = None, None
                        last_time, play_time = None, None

                        # Get values from database
                        data = self.database.get("games",
                            { "filename": basename(game) })

                        if data is not None:

                            # Favorite
                            if bool(data["favorite"]):
                                icon = self.icons["favorite"]
                                favorite = True

                            # Custom name
                            if len(data["name"]) > 0:
                                custom_name = data["name"]

                            # Played
                            if data["play"] > 0:
                                number = str(data["play"])

                            # Last time
                            if len(data["last_play"]) > 0:
                                last = self.game_time_since_last_play(
                                    data["last_play"])

                            # Last play time
                            if len(data["last_play_time"]) > 0:
                                last_time = data["last_play_time"]

                            # Play time
                            if len(data["play_time"]) > 0:
                                play_time = data["play_time"]

                            # Multiplayer
                            if bool(data["multiplayer"]):
                                multiplayer = self.icons["multiplayer"]

                            # Exception
                            if len(data["arguments"]) > 0 or \
                                len(data["emulator"]) > 0:
                                exception = self.icons["except"]

                        # Installed time
                        installed = self.game_time_since_last_play(str(
                            datetime.fromtimestamp(getctime(game)).strftime(
                            "%d-%m-%Y %H:%M:%S")))

                        # Snap
                        snap = self.empty
                        if self.game_check_snap(emulator, filename):
                            snap = self.icons["snap"]

                        # Save state
                        save = self.empty
                        if self.game_check_save_states(emulator, filename):
                            save = self.icons["save"]

                        row = self.model_games.append([favorite, icon,
                            custom_name, number, last, last_time, play_time,
                            installed, exception, snap, multiplayer, save,
                            filename])

                        self.game_path[filename] = [game, row]

                        iteration += 1
                        if (iteration % 20 == 0):
                            self.statusbar_update()

                            self.treeview_games.thaw_child_notify()
                            yield True
                            self.treeview_games.freeze_child_notify()

            # Restore options for packages treeviews
            self.treeview_games.set_enable_search(True)
            self.treeview_games.thaw_child_notify()

        self.statusbar_update()

        self.sensitive_interface()

        # ------------------------------------
        #   Close thread
        # ------------------------------------

        self.list_thread = int()

        yield False


    def game_check_save_states(self, emulator, gamename):
        """
        Check if a game has some save states
        """

        if not emulator in self.emulators.sections():
            return False

        if self.emulators.has_option(emulator, "save"):
            save_path = expanduser(self.emulators.get(emulator, "save"))

            if emulator in self.emulators.sections():
                pattern = save_path.replace("<name>", gamename)

                if len(glob(pattern)) > 0:
                    return True

        return False


    def game_check_snap(self, emulator, gamename):
        """
        Check if a game has some snaps
        """

        if not emulator in self.emulators.sections():
            return False

        if self.emulators.has_option(emulator, "snaps"):
            snaps_path = expanduser(self.emulators.get(emulator, "snaps"))

            if emulator in self.emulators.sections():
                if "<lname>" in snaps_path:
                    pattern = snaps_path.replace("<lname>", gamename).lower()
                else:
                    pattern = snaps_path.replace("<name>", gamename)

                if len(glob(pattern)) > 0:
                    return True

        return False


    def __on_game_selected(self, treeview, event):
        """
        Select a console in consoles treeview
        """

        name, snap, run_game = None, None, None

        # Keyboard
        if event.type == gtk.gdk.KEY_RELEASE:

            model, treeiter = treeview.get_selection().get_selected()
            if treeiter is not None:
                name = model.get_value(treeiter, Columns.Filename)

                if gtk.gdk.keyval_name(event.keyval) == "Return":
                    run_game = True

        # Mouse
        elif (event.type in [gtk.gdk.BUTTON_PRESS, gtk.gdk._2BUTTON_PRESS]) \
            and (event.button == 1 or event.button == 3):

            selection = treeview.get_path_at_pos(int(event.x), int(event.y))
            if selection is not None:
                model = treeview.get_model()

                treeiter = model.get_iter(selection[0])
                name = model.get_value(treeiter, Columns.Filename)

                if event.button == 1 and event.type == gtk.gdk._2BUTTON_PRESS:
                    run_game = True

        # ----------------------------
        #   Game selected
        # ----------------------------

        if name is not None:
            self.selection["game"] = self.game_path[name][0]

            self.sensitive_interface(True)

            # ----------------------------
            #   Game log
            # ----------------------------

            log_path = path_join(expanduser(Conf.Data),
                "logs/%s.log" % basename(self.selection["game"]))

            if exists(log_path):
                with open(log_path, 'r') as pipe:
                    self.log.get_buffer().set_text(''.join(pipe.readlines()))

            else:
                self.log.get_buffer().set_text(str())

            # ----------------------------
            #   Game data
            # ----------------------------

            if model.get_value(treeiter, Columns.Snapshots) == self.empty:
                self.item_snapshots.set_sensitive(False)
                self.item_game_snapshots.set_sensitive(False)
                self.tool_snapshots.set_sensitive(False)

            if model.get_value(treeiter, Columns.Except) == self.empty:
                self.item_game_parameters.set_label(_("_Set parameters"))
            else:
                self.item_game_parameters.set_label(_("_Modify parameters"))

            if run_game:
                self.__on_game_launch()

        self.statusbar_update()


    def __on_game_marked_as_favorite(self, widget):
        """
        Mark or unmark a game as favorite
        """

        gamefile = basename(self.selection["game"])
        gamename = splitext(gamefile)[0]

        treeiter = self.game_path[gamename][1]

        if self.model_games[treeiter][Columns.Icon] == self.empty:
            self.model_games[treeiter][Columns.Favorite] = True
            self.model_games[treeiter][Columns.Icon] = self.icons["favorite"]

            self.database.modify("games",
                { "favorite": 1 }, { "filename": gamefile })

        else:
            self.model_games[treeiter][Columns.Favorite] = False
            self.model_games[treeiter][Columns.Icon] = self.empty

            self.database.modify("games",
                { "favorite": 0 }, { "filename": gamefile })


    def __on_game_marked_as_multiplayer(self, widget):
        """
        Mark or unmark a game as multiplayer
        """

        gamefile = basename(self.selection["game"])
        gamename = splitext(gamefile)[0]

        treeiter = self.game_path[gamename][1]

        if self.model_games[treeiter][Columns.Multiplayer] == self.empty:
            self.model_games[treeiter][Columns.Multiplayer] = \
                self.icons["multiplayer"]

            self.database.modify("games",
                { "multiplayer": 1 }, { "filename": gamefile })

        else:
            self.model_games[treeiter][Columns.Multiplayer] = self.empty

            self.database.modify("games",
                { "multiplayer": 0 }, { "filename": gamefile })


    def __on_game_clean(self, widget):
        """
        Remove game entry from database
        """

        gamefile = basename(self.selection["game"])
        gamename = splitext(gamefile)[0]

        treeiter = self.game_path[gamename][1]

        dialog = Dialog.Question(self, _("Remove %s") % gamename,
            _("Would you really want to remove this game from database ?"))

        if dialog.run() == gtk.RESPONSE_YES:
            self.model_games[treeiter][Columns.Name] = gamename
            self.model_games[treeiter][Columns.Favorite] = False
            self.model_games[treeiter][Columns.Icon] = self.empty
            self.model_games[treeiter][Columns.Played] = None
            self.model_games[treeiter][Columns.LastPlay] = None
            self.model_games[treeiter][Columns.TimePlay] = None
            self.model_games[treeiter][Columns.LastTimePlay] = None
            self.model_games[treeiter][Columns.Except] = None

            self.database.remove("games", { "filename": gamefile })

        dialog.destroy()


    def __on_game_removed(self, widget):
        """
        Remove game files from harddrive
        """

        file_to_remove = list()

        need_to_reload = False

        emulator = self.consoles.get(self.selection["console"], "emulator")

        gamefile = basename(self.selection["game"])
        gamename = splitext(gamefile)[0]

        treeiter = self.game_path[gamename][1]

        dialog = Window.Remove(self,
            _("Remove %s") % self.model_games[treeiter][Columns.Name],
            _("You going to remove this game from disk."))

        if dialog.run() == gtk.RESPONSE_OK:
            file_to_remove.append(self.selection["game"])

            if dialog.check_database.get_active():
                self.database.remove("games", { "filename": gamefile })

            if dialog.check_save.get_active():
                if self.emulators.has_option(emulator, "save"):
                    path = expanduser(self.emulators.get(emulator, "save"))

                    if emulator in self.emulators.sections():
                        file_to_remove.extend(
                            glob(path.replace("<name>", gamename)))

            if dialog.check_screenshots.get_active():
                if self.emulators.has_option(emulator, "snaps"):
                    path = expanduser(self.emulators.get(emulator, "snaps"))

                    if emulator in self.emulators.sections():
                        if "<lname>" in path:
                            pattern = path.replace("<lname>", gamename).lower()
                        else:
                            pattern = path.replace("<name>", gamename)

                        file_to_remove.extend(glob(pattern))

            for element in file_to_remove:
                remove(element)

            need_to_reload = True

        dialog.destroy()

        if need_to_reload:
            self.load_interface()

            Dialog.Message(self,
                _("Remove %s") % self.model_games[treeiter][Columns.Name],
                _("This game was removed successfully")).start()


    def __on_game_copy(self, widget):
        """
        Copy game directory to clipboard
        """

        self.clipboard.set_text(self.selection["game"])


    def __on_game_open(self, widget):
        """
        Open game directory in default files manager

        http://stackoverflow.com/a/6631329
        """

        path = dirname(self.selection["game"])

        if system() == "Windows":
            from os import startfile
            startfile(path)

        elif system() == "Darwin":
            Popen(["open", path])

        else:
            Popen(["xdg-open", path])


    def game_update_data(self, index, icon=None):
        """
        Update game save state
        """

        if icon is None:
            icon = self.empty

        model, treeiter = self.treeview_games.get_selection().get_selected()
        if treeiter is not None:
            treeiter = model.convert_iter_to_child_iter(treeiter)

            if treeiter is not None:
                self.model_games[treeiter][index] = icon


    def __on_game_launch(self, widget=None):
        """
        Start a game
        """

        no_error = True

        binary = str()

        if self.selection["game"] is None:
            return

        filename = basename(self.selection["game"])
        game = self.database.get("games", { "filename": filename })

        console = self.selection["console"]

        if game is not None and len(game.get("emulator")) > 0:
            emulator = game.get("emulator")
        else:
            emulator = self.consoles.get(console, "emulator")

        if emulator is not None and emulator in self.emulators.sections():

            # ----------------------------
            #   Check emulator binary
            # ----------------------------

            binary = self.emulators.get(emulator, "binary")

            # ----------------------------
            #   Default arguments
            # ----------------------------

            args = str()

            if self.emulators.has_option(emulator, "default"):
                args = self.emulators.get(emulator, "default")

            exceptions = self.database.select("games", "arguments",
                { "filename": filename })
            if exceptions is not None and len(exceptions) > 0:
                args = exceptions

            # ----------------------------
            #   Set fullscreen mode
            # ----------------------------

            # Fullscreen
            if self.tool_fullscreen.get_active():
                if self.emulators.has_option(emulator, "fullscreen"):
                    args += " %s" %self.emulators.get(emulator, "fullscreen")

            # Windowed
            else:
                if self.emulators.has_option(emulator, "windowed"):
                    args += " %s" %self.emulators.get(emulator, "windowed")

            # ----------------------------
            #   Generate correct command
            # ----------------------------

            command = list()

            # Append binaries
            command.extend(shlex_split(binary))

            # Append arguments
            if args is not None:
                command.extend(shlex_split(args))

            # Append game file
            command.append(self.selection["game"])

            # ----------------------------
            #   Run game
            # ----------------------------

            self.set_sensitive(False)

            date_start = datetime.now()

            try:
                self.proc = Popen(command, stdout=PIPE, stdin=PIPE,
                    universal_newlines=True)
                self.proc.wait()

            except OSError as error:
                no_error = False

                dialog = Dialog.Error(self,
                    _("Missing binary"), _("Cannot found <b>%s</b> !" % binary))

                dialog.start()

            except KeyboardInterrupt as error:
                pass

            # ----------------------------
            #   Save game data
            # ----------------------------

            if no_error:
                output, error_output = self.proc.communicate()

                log_buffer = self.log.get_buffer()
                log_buffer.set_text(str())

                log_buffer.insert(log_buffer.get_end_iter(),
                    "%s\n\n" % " ".join(command))
                log_buffer.insert(log_buffer.get_end_iter(), output)

                start, end = log_buffer.get_bounds()

                # Write output into game's log
                with open(path_join(expanduser(Conf.Data),
                    "logs/%s.log" % filename), 'w') as pipe:
                    pipe.write(log_buffer.get_text(start, end))

                # Calc time since game start
                interval = datetime.now() - date_start

                total = self.game_calc_play_time(filename, interval)
                play_time = self.game_calc_play_time(filename, interval, False)

                # ----------------------------
                #   Update data
                # ----------------------------

                gamename = splitext(filename)[0]

                # Play data
                self.database.modify("games", {
                    "play_time": total,
                    "last_play": datetime.now().strftime("%d-%m-%Y %H:%M:%S"),
                    "last_play_time": play_time,
                    }, { "filename": filename })

                # Set new data into games treeview
                value = self.database.get("games", { "filename": filename })
                if value is not None:

                    # Last played
                    self.game_update_data(Columns.LastPlay,
                        self.game_time_since_last_play(value["last_play"]))

                    # Last time played
                    self.game_update_data(Columns.LastTimePlay,
                        value["last_play_time"])

                    # Play time
                    self.game_update_data(Columns.TimePlay, value["play_time"])

                    # Played
                    if value["play"] is None:
                        play = 1
                    else:
                        play = value["play"] + 1

                    self.database.modify("games",
                        { "play": play }, { "filename": filename })
                    self.game_update_data(Columns.Played, play)

                # Snaps
                if self.game_check_snap(emulator, gamename):
                    self.game_update_data(Columns.Snapshots, self.icons["snap"])
                    self.tool_snapshots.set_sensitive(True)
                else:
                    self.game_update_data(Columns.Snapshots)

                # Save state
                if self.game_check_save_states(emulator, gamename):
                    self.game_update_data(Columns.Save, self.icons["save"])
                else:
                    self.game_update_data(Columns.Save)

            self.set_sensitive(True)


    def __on_game_snaps(self, widget):
        """
        Open snaps from a specific game with feh
        """

        viewer = self.config.get("viewer", "binary")
        args = self.config.item("viewer", "options")

        if exists(viewer):
            emulator = self.consoles.get(self.selection["console"], "emulator")
            gamename = basename(self.selection["game"]).split('.')[0]

            if self.game_check_snap(emulator, gamename):
                snaps_path = expanduser(self.emulators.get(emulator, "snaps"))

                if "<lname>" in snaps_path:
                    path = glob(
                        snaps_path.replace("<lname>", gamename).lower())
                else:
                    path = glob(snaps_path.replace("<name>", gamename))

                # ----------------------------
                #   Show screenshots viewer
                # ----------------------------

                if bool(int(self.config.item("viewer", "native", '1'))):
                    Window.Screenshots(self, _("%s screenshots" % gamename),
                        path)

                else:
                    command = list()

                    # Append binaries
                    command.extend(shlex_split(viewer))

                    # Append arguments
                    if args is not None:
                        command.extend(shlex_split(args))

                    # Append game file
                    command.extend(path)

                    process = Popen(command)
                    process.wait()

                # ----------------------------
                #   Check snapshots
                # ----------------------------

                if not self.game_check_snap(emulator, gamename):
                    self.game_update_data(Columns.Snapshots)

        else:
            self.set_error(_("Cannot find <b>%s</b> viewer !" % viewer),
                gtk.MESSAGE_WARNING)


    def __on_game_parameters(self, widget):
        """
        Set some parameters for a specific game
        """

        filename = basename(self.selection["game"])

        parameters = self.database.select("games", ["arguments", "emulator"],
            { "filename": filename })

        dialog = Window.ROM(self, filename,
            _("Set specific parameters for this ROM"), parameters,
            self.emulators.sections())

        if dialog.run() == gtk.RESPONSE_OK:

            # ----------------------------
            #   Update exceptions
            # ----------------------------

            self.database.modify("games", {
                    "arguments": dialog.entry_arguments.get_text(),
                    "emulator": dialog.combo_emulator.get_active_text()
                }, { "filename": filename })

            # ----------------------------
            #   Update data
            # ----------------------------

            result = self.database.select("games", ["arguments", "emulator"],
                { "filename": filename })

            if result is not None and \
                (len(result[0]) > 0 or len(result[1]) > 0):
                self.game_update_data(Columns.Except, self.icons["except"])
            else:
                self.game_update_data(Columns.Except)

        dialog.destroy()


    def __on_game_renamed(self, widget):
        """
        Set a custom name for a specific game
        """

        gamefile = basename(self.selection["game"])
        gamename = splitext(gamefile)[0]

        treeiter = self.game_path[gamename][1]

        result = self.database.get("games", { "filename": gamefile })
        if not result is None and len(result["name"]) > 0:
            gamename = result["name"]

        dialog = Dialog.Input(
            self, gamefile, _("Set a custom name"), gamename)

        if dialog.run() == gtk.RESPONSE_OK and \
            not dialog.entry.get_text() == result and \
            len(dialog.entry.get_text()) > 0:
            self.model_games[treeiter][Columns.Name] = dialog.entry.get_text()

            self.database.modify("games",
                { "name": dialog.entry.get_text() }, { "filename": gamefile })

        dialog.destroy()


    def game_time_since_last_play(self, date):
        """
        Get time since last play
        """

        if date is not None:

            now = datetime.now()
            game_date = datetime.strptime(date, "%d-%m-%Y %H:%M:%S")

            value = (now - game_date).days

            if value == 0:
                return _("Today")
            elif value == 1:
                return _("Yesterday")
            elif value < 30:
                return _("%d days ago") % value
            else:
                months = int(value / 30)

                if months == 1:
                    return _("Last month")
                elif months < 12:
                    return _("%d months ago") % months
                else:
                    years = int(value / 365)

                    if years == 1:
                        return _("Last year")

                    return _("%d years ago") % years

        return None


    def game_calc_play_time(self, gamename, interval, total=True):
        """
        Calc time passed ingame
        """

        result = self.database.get("games", { "filename": gamename })

        if result is not None and total and len(result["play_time"]) > 0:
            play_time = datetime.strptime(result["play_time"], "%H:%M:%S")
            play_time += interval

        else:
            play_time = interval

        return str(play_time).split('.')[0].split()[-1]


    def icon_load(self, name, size=16, fallback="image-missing"):
        """
        Get an icon from data folder
        """

        # Check if specific icon name is in icons theme
        if self.icons_theme.has_icon(name):
            try:
                return self.icons_theme.load_icon(name, size,
                    gtk.ICON_LOOKUP_FORCE_SVG)

            except:
                if type(fallback) == Pixbuf:
                    return fallback

                return self.icons_theme.load_icon(fallback, size,
                    gtk.ICON_LOOKUP_FORCE_SVG)

        # Return fallback icon (in the case where is a Pixbuf)
        if type(fallback) == Pixbuf:
            return fallback

        # Find fallback icon in icons theme
        if self.icons_theme.has_icon(fallback):
            return self.icons_theme.load_icon(fallback, size,
                gtk.ICON_LOOKUP_FORCE_SVG)

        # Instead, return default image
        return self.icons_theme.load_icon("image-missing", size,
            gtk.ICON_LOOKUP_FORCE_SVG)


    def filters_update(self, widget=None):
        """
        Reload packages filter when user change filters from menu
        """

        self.filter_games.refilter()


    def filters_match(self, model, row, data=None):
        """
        Update treeview filter
        """

        data_favorite = model.get_value(row, Columns.Favorite)
        flag_favorite = self.tool_filter_favorite.get_active()

        data_multiplayer = model.get_value(row, Columns.Multiplayer)
        flag_multiplayer = self.tool_filter_multiplayer.get_active()

        name = model.get_value(row, Columns.Name)
        if name is not None:
            text = self.entry_filter.get_text()

            # No flag
            if not flag_favorite and not flag_multiplayer:
                if len(text) == 0 or text.lower() in name.lower():
                    return True

            # Only favorite flag
            if flag_favorite and data_favorite and not flag_multiplayer:
                if len(text) == 0 or text.lower() in name.lower():
                    return True

            # Only multiplayer flag
            if flag_multiplayer and not data_multiplayer == self.empty and \
                not flag_favorite:
                if len(text) == 0 or text.lower() in name.lower():
                    return True

            # Both favorite and multiplayer flags
            if flag_favorite and data_favorite and \
                flag_multiplayer and not data_multiplayer == self.empty:
                if len(text) == 0 or text.lower() in name.lower():
                    return True

        return False


    def set_error(self, message, message_type=gtk.MESSAGE_ERROR):
        """
        Show or hide informations bar when error occur
        """

        self.label_informations.set_markup(message)
        self.informations.set_message_type(message_type)

        self.informations.show_all()


    def __on_activate_fullscreen(self, widget):
        """
        Update fullscreen icon
        """

        if self.tool_fullscreen.get_active():
            self.tool_fullscreen.set_stock_id(gtk.STOCK_LEAVE_FULLSCREEN)

        else:
            self.tool_fullscreen.set_stock_id(gtk.STOCK_FULLSCREEN)


class AboutDialog(gtk.AboutDialog):

    def __init__(self, widget, parent):
        """
        Constructor
        """

        gtk.AboutDialog.__init__(self)

        self.set_name(parent.application_title)
        self.set_version(parent.application_version)
        self.set_logo_icon_name(Icons.Game)
        self.set_comments(_("A graphical tool to manage emulators"))
        self.set_website("https://pacmiam.tuxfamily.org")
        self.set_copyright("Copyleft 2016 - Kawa Team")

        # Authors
        self.set_authors([
            _("Active developpers"),
            "\tAurélien Lubert <pacmiam@tuxfamily.org>", "",
            _("Translators"),
            "\t%s - José Luis <darknekros@gmail.com>" % _("Spanish"),
        ])

        # Arts
        self.set_artists(
            [_("Icons"),
            "\tTango projects\tGPLv3",
            "\tGelide projects\tGPLv3",
            "\tEvan-Amos\t\tCC-by-SA 3.0"])

        # License
        self.set_license("gem is free software: you can redistribute it "
            "and/or modify it under the terms of the GNU General Public "
            "License as published by the Free Software Foundation, either "
            "version 3 of the License, or (at your option) any later version."
            "\n\ngem is distributed in the hope that it will be useful, but "
            "WITHOUT ANY WARRANTY; without even the implied warranty of "
            "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU "
            "General Public License for more details.\n\n"
            "http://www.gnu.org/licenses/")
        self.set_wrap_license(True)

        self.run()

        self.destroy()


class SplashWindow(gtk.Window):

    def __init__(self, title, subtitle, length):
        """
        Init splash window
        """

        gtk.Window.__init__(self, gtk.WINDOW_TOPLEVEL)

        # ------------------------------------
        #   Variables
        # ------------------------------------

        self.window_title, self.subtitle, self.length = title, subtitle, length

        self.index = int(1)

        # ------------------------------------
        #   Initialization
        # ------------------------------------

        # Init widgets
        self.__init_widgets()

        # Start interface
        self.__start_interface()


    def __init_widgets (self):
        """
        Load widgets into main interface
        """

        self.set_title(self.window_title)

        self.set_decorated(False)
        self.set_default_size(500, 200)
        self.set_skip_taskbar_hint(True)
        self.set_position(gtk.WIN_POS_CENTER)

        self.set_wmclass("gem", "gem")

        # ------------------------------------
        #   Grids
        # ------------------------------------

        self.vbox = gtk.VBox()

        # Properties
        self.vbox.set_border_width(8)

        # ------------------------------------
        #   Widgets
        # ------------------------------------

        label_title = gtk.Label()

        self.spinner = gtk.Spinner()

        label_subtitle = gtk.Label()

        self.progressbar = gtk.ProgressBar()

        # Properties
        label_title.set_use_markup(True)
        label_title.set_markup("<span font='20'><b>%s</b></span>" % (
            self.window_title))

        label_subtitle.set_text(self.subtitle)

        self.progressbar.set_text(str())

        # ------------------------------------
        #   Add widgets into main window
        # ------------------------------------

        self.vbox.pack_start(label_title, False, False)
        self.vbox.pack_start(self.spinner, padding=16)
        self.vbox.pack_start(label_subtitle, False, False)
        self.vbox.pack_start(self.progressbar, False, False)

        self.add(self.vbox)


    def __start_interface(self):
        """
        Load data and start interface
        """

        self.show_all()

        self.spinner.start()

        self.refresh()


    def update(self):
        """
        Update splash widgets
        """

        self.refresh()

        if self.index <= self.length:
            self.progressbar.set_text("%d / %d" % (self.index, self.length))
            self.progressbar.set_fraction(float(self.index) / (self.length))

            self.index += 1

            self.refresh()


    def refresh(self):
        """
        Refresh all pendings event in main interface
        """

        while gtk.events_pending():
            gtk.main_iteration()

